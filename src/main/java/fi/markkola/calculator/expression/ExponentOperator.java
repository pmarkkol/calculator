/* ExponentOperator.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator.expression;

/**
 * This class represents an exponent operator.
 *
 * @author Paavo Markkola
 */
public class ExponentOperator extends Operator {
	/**
	 * String presentation of the exponent operator.
	 */
	private static String operator = "^";
	/**
	 * Static method that returns string presentation of the exponent operator.
	 *
	 * @return	String presentation of the exponent operator.
	 */
	public static String getOperator() {
		return operator;
	}
	/**
	 * Return precedence value of the exponent operator.
	 *
	 * @return	Precedence value of the exponent operator.
	 */
	@Override
	public int precedenceValue() {
		return 3;
	}
	/**
	 * Execute the exponent operator for two operand that are given as
	 * parameter, and return the results.
	 *
	 * @param second	Second operand for the operator.
	 * @param first		First operand for the operator.
	 * @return	Result of the operation.
	 */
	@Override
	public double execute(double second, double first) {
		return Math.pow(first, second);
	}
}

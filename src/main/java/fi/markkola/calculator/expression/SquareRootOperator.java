/* SquareRootOperator.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator.expression;

/**
 * This class represents a square root operator.
 *
 * @author Paavo Markkola
 */
public class SquareRootOperator extends Operator {
	/**
	 * String presentation of the square root operator.
	 */
	private static String operator = "sqr";
	/**
	 * Static method that returns string presentation of the square root
	 * operator.
	 *
	 * @return	String presentation of the square root operator.
	 */
	public static String getOperator() {
		return operator;
	}
	/**
	 * Return precedence value of the square root operator.
	 *
	 * @return	Precedence value of the square root operator.
	 */
	@Override
	public int precedenceValue() {
		return 4;
	}
	/**
	 * Execute the square root operator for one operand that is given as
	 * parameter, and return the results.
	 *
	 * @param operand	Operand for the operator.
	 * @return	Result of the operation.
	 */
	@Override
	public double execute(double operand) {
		return Math.sqrt(operand);
	}
}

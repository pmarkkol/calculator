/* ExpressionSolver.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator.expression;

/**
 * This interface defines a class that parses and evaluates mathematical
 * expressions.
 *
 * @author Paavo Markkola
 */
public interface ExpressionSolver {
	/**
	 * Method to get instructions on how to use the expression solver. This
	 * includes syntax for the expressions and available operators.
	 *
	 * @return Instructions for the user in String presentation.
	 */
	String getUserInstructions();
	/**
	 * Method to parse a mathematical expression. Parsing status can be later
	 * read with getStatus method. If parsing is successful the expression
	 * can then be evaluated with solveEquation method.
	 *
	 * @param expression	String presentation of a mathematical expression.
	 * @throws InvalidExpressionException	Exception indicating an error
	 * 										during parsing.
	 */
	void parseExpression(String expression) throws InvalidExpressionException;
	/**
	 * Method to evaluate the mathematical expression previously parsed with
	 * parseExpression method. Requires variable value as parameter and returns
	 * evaluation results as double.
	 *
	 * @param value	Numerical value used for variables in the expression.
	 * @return	Evaluation result, or NaN if the expression cannot be evaluated.
	 */
	double evaluateExpression(double value);
	/**
	 * Method to check error status of the last parse or evaluate operation.
	 *
	 * @return	True if the operation was successful, false otherwise.
	 */
	boolean getStatus();
	/**
	 * Method to check position of an error if a parse operation has failed.
	 * The position is the index of the character causing the parse error in
	 * the string given as parameter to the parse method. Return value is valid
	 * only if the parse operation has failed.
	 *
	 * @return	Position of an error in the expression string.
	 */
	int getPosition();
	/**
	 * Method to check error message after a failed parse or evaluate operation.
	 *
	 * @return	Error message describing the reason for the error.
	 */
	String getMessage();
}

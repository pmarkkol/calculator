/* UnaryComponent.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator.expression;

/**
 * This class represents an unary operator component of a mathematical
 * expression. Unary operators are operators that require only one operand,
 * e.g., square root, modulus and such.
 *
 * @author Paavo Markkola
 */
public class UnaryComponent extends Component {
	/**
	 * Instruction string for the user describing how to use the feature.
	 */
	private static String instructions =
		  "<span class=\"ins-strong\">Unary operators</span>. These "
		+ "operators act on one operand directly following the operator. Any "
		+ "number of spaces can be included with unary operators. Valid unary "
		+ "operators are <span class=\"ins-code\">abs</span>, "
		+ "<span class=\"ins-code\">sqr</span>, "
		+ "<span class=\"ins-code\">sin</span>, "
		+ "<span class=\"ins-code\">cos</span>, "
		+ "<span class=\"ins-code\">tan</span> and unary signs "
		+ "<span class=\"ins-code\">-</span> and "
		+ "<span class=\"ins-code\">+</span>. Note "
		+ "that operators <span class=\"ins-code\">-</span> and "
		+ "<span class=\"ins-code\">+</span> can be either unary or binary "
		+ "operator depending on context, e.g., "
		+ "<span class=\"ins-code\">x--1</span> is the same as "
		+ "<span class=\"ins-code\">x-(-1)</span>.";
	/**
	 * Operator instance that actually perform the functionality of the unary
	 * operator.
	 */
	private Operator operator;
	/**
	 * Regular expression for unary components.
	 */
	public static String unaryOperatorRegEx = "(^\\s*sqr\\s*)|" +
											  "(^\\s*abs\\s*)|" +
											  "(^\\s*sin\\s*)|" +
											  "(^\\s*cos\\s*)|" +
											  "(^\\s*tan\\s*)";
	/**
	 * Construct a new UnaryComponent. In case of unary signs, - and +, change
	 * the string presentation to distinct them from binary operators with the
	 * same presentation. Set correct Operator instance for the unary operator.
	 *
	 * @param component	String presentation of the component.
	 * @param position	Position of the component in a mathematical expression.
	 */
	public UnaryComponent(String component, int position) {
		super(component, position);
		setUnarySignString();
		setOperator();
	}
	/**
	 * Format and return user instructions string.
	 *
	 * @return Instructions for the user in String presentation.
	 */
	public static String getUserInstructions() {
		return instructions;
	}
	/**
	 * In case of unary signs, - and +, change the string presentation to
	 * distinct them from binary operators with the same presentation.
	 */
	private void setUnarySignString() {
		if (cString.getComponentString().equals("-") ||
			cString.getComponentString().equals("+"))
			cString.setComponentString(cString.getComponentString() + "'");
	}
	/**
	 * Set the corresponding Operator instance.
	 */
	private void setOperator() {
		for (OperatorType type : OperatorType.values())
			if (cString.getComponentString().equals(type.getOperator()))
				operator = type.create();
	}
	/**
	 * Return regular expression for unary components.
	 *
	 * @return	Regular expression.
	 */
	public static String getRegEx() {
		return unaryOperatorRegEx;
	}
	/**
	 * This component is a unary operator so return true.
	 *
	 * @return	True.
	 */
	@Override
	public boolean isUnaryOperator() {
		return true;
	}
	/**
	 * Unary operator cannot be at last position so return false.
	 *
	 * @return	False.
	 */
	@Override
	public boolean canBeLast() {
		return false;
	}
	/**
	 * Return precedence value of the unary operator.
	 *
	 * @return	Precedence value of the unary operator.
	 */
	@Override
	public int precedenceValue() {
		return operator.precedenceValue();
	}
	/**
	 * Execute the unary operation with the operand given as parameter.
	 *
	 * @param operand	Operand for the operator.
	 * @return	Result of the operation.
	 */
	@Override
	public double execute(double operand) {
		return operator.execute(operand);
	}
}

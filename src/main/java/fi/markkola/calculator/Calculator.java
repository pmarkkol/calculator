/* Calculator.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator;

/**
 * Simple java application to plot graphs from mathematical expressions.
 *
 * @author Paavo Markkola
 */
public class Calculator {
	/**
	 * Launch the application.
	 *
	 * @param args	Command line arguments.
	 */
	public static void main(String[] args) {
		new CalculatorGUI();
	}
}

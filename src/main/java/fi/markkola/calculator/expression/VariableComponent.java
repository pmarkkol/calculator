/* VariableComponent.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator.expression;

/**
 * This class represents a variable component of a mathematical expression.
 * A mathematical variable an alphabetic character which is replaced with a
 * number during evaluation of the expression.
 *
 * @author Paavo Markkola
 */
public class VariableComponent extends Component {
	/**
	 * Instruction string for the user describing how to use the feature.
	 */
	private static String instructions =
		  "<span class=\"ins-strong\">Variable</span>, i.e., an alphabetic "
	    + "character representing a number. Value of the variable is defined "
	    + "at evaluation phase. The only valid variable identifier is "
	    + "<span class=\"ins-code\">x</span>. Both lower and upper case "
	    + "letters can be used.";
	/**
	 * Numerical value of the component.
	 */
	private double value = Double.NaN;
	/**
	 * Regular expression for variable components.
	 */
	public static String variableRegEx = "(^\\s*[xX]\\s*)";
	/**
	 * Construct a new VariableComponent.
	 *
	 * @param component	String presentation of the component.
	 * @param position	Position of the component in a mathematical expression.
	 */
	public VariableComponent(String component, int position) {
		super(component, position);
	}
	/**
	 * Format and return user instructions string.
	 *
	 * @return Instructions for the user in String presentation.
	 */
	public static String getUserInstructions() {
		return instructions;
	}
	/**
	 * Return regular expression for variable components.
	 *
	 * @return	Regular expression.
	 */
	public static String getRegEx() {
		return variableRegEx;
	}
	/**
	 * This component is a variable so return true.
	 *
	 * @return	True.
	 */
	@Override
	public boolean isVariable() {
		return true;
	}
	/**
	 * Return the numerical value of the variable component.
	 *
	 * @return	Numerical value of the constant.
	 */
	@Override
	public double getValue() {
		return value;
	}
	/**
	 * Set numerical value for the variable component.
	 *
	 * @param value	Numerical value for the variable component.
	 */
	@Override
	public void setValue(double value) {
		this.value = value;
	}
}

/* ConstantComponent.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator.expression;

/**
 * This class represents a constant component in a mathematical expression.
 * A mathematical constant is a number that is significantly interesting in
 * some way, e.g., pi.
 *
 * @author Paavo Markkola
 */
public class ConstantComponent extends Component {
	/**
	 * Instruction string for the user describing how to use the feature.
	 */
	private static String instructions =
		  "<span class=\"ins-strong\">Mathematical constant</span>, i.e., a "
		+ "significant and well defined number of constant value. Valid "
		+ "constants are Pi (<span class=\"ins-code\">pi</span>) and Euler\'s "
		+ "constant (<span class=\"ins-code\">e</span>).";
	/**
	 * Numerical value of the component.
	 */
	private double value;
	/**
	 * Regular expression for constant components.
	 */
	public static String constantRegEx = "(^\\s*e\\s*)|(^\\s*pi\\s*)";
	/**
	 * Construct a new ConstantComponent. Store the numerical value of the
	 * constant.
	 *
	 * @param component	String presentation of the component.
	 * @param position	Position of the component in a mathematical expression.
	 */
	public ConstantComponent(String component, int position) {
		super(component, position);
		setNumericalValue();
	}
	/**
	 * Format and return user instructions string.
	 *
	 * @return Instructions for the user in String presentation.
	 */
	public static String getUserInstructions() {
		return instructions;
	}
	/**
	 * Find out which mathematical constant the component represents and
	 * convert it to a numerical value and store it.
	 */
	private void setNumericalValue() {
		if (cString.getComponentString().equals("e"))
			value = Math.E;
		else if (cString.getComponentString().equals("pi"))
			value = Math.PI;
		else
			value = Double.NaN;
	}
	/**
	 * Return regular expression for constant components.
	 *
	 * @return	Regular expression.
	 */
	public static String getRegEx() {
		return constantRegEx;
	}
	/**
	 * This component is a constant so return true.
	 *
	 * @return	True.
	 */
	@Override
	public boolean isConstant() {
		return true;
	}
	/**
	 * Return the numerical value of the constant component.
	 *
	 * @return	Numerical value of the constant.
	 */
	@Override
	public double getValue() {
		return value;
	}
}

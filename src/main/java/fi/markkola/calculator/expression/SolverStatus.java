/* SolverStatus.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator.expression;

/**
 * This class holds the error status of an ExpressionSolver implementation.
 *
 * @author Paavo Markkola
 */
public class SolverStatus {
	private boolean status;
	private int position;
	private String message;
	/**
	 * Construct a new SolverStatus with default values.
	 */
	public SolverStatus() {
		set(false, 0, "");
	}
	/**
	 * Construct a new SolverStatus with parameters.
	 *
	 * @param status	Error status.
	 * @param position	Error position.
	 * @param message	Error message.
	 */
	public SolverStatus(boolean status, int position, String message) {
		set(status, position, message);
	}
	/**
	 * Set new values for the status.
	 *
	 * @param status	New error status.
	 * @param position	New error position.
	 * @param message	New error message.
	 */
	public void set(boolean status, int position, String message) {
		this.status = status;
		this.position = position;
		this.message = message;
	}
	/**
	 * Return status.
	 *
	 * @return	Error status.
	 */
	public boolean getStatus() {
		return status;
	}
	/**
	 * Return position related to the status.
	 *
	 * @return	Error position.
	 */
	public int getPosition() {
		return position;
	}
	/**
	 * Return message related to the status.
	 *
	 * @return	Error message.
	 */
	public String getMessage() {
		return message;
	}
}
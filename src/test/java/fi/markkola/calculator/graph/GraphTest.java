package fi.markkola.calculator.graph;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fi.markkola.calculator.expression.EvaluationErrorException;
import fi.markkola.calculator.expression.InvalidExpressionException;
import fi.markkola.calculator.expression.PostfixSolver;

public class GraphTest {
	private static final double DELTA = 1e-15;

	private Graph graph;
	private PostfixSolver postfixSolver;
	private GraphLimits graphLimits;

	@Before
	public void setUp() {
    	 graph = new Graph();
    	 postfixSolver = new PostfixSolver();
    	 graphLimits = new GraphLimits();
	}

	@After
	public void tearDown() {
    	 graph = null;
    	 postfixSolver = null;
    	 graphLimits = null;
	}

	@Test
	public void testCreateEmptyGraph() {
		assertNotNull(graph);
		assertNull(graph.getExpressionSolver());
	}

	@Test
	public void testGraphSetExpressionSolver() {
		// Test that ExpressionSolver can be set.
		assertNotNull(postfixSolver);
		graph.setExpressionSolver(postfixSolver);
		assertEquals(postfixSolver, graph.getExpressionSolver());
	}

	@Test
	public void testGraphSetXAxis() {
		// Test setting all positive limits for x axis.
		graphLimits.setLimitsForX(10.0, 20.0);
		graph.setGraphLimits(graphLimits);
		assertEquals(10.0, graph.getGraphLimits().getMinX(), DELTA);
		assertEquals(20.0, graph.getGraphLimits().getMaxX(), DELTA);
		// Test setting limits on both sides of zero for x axis.
		graphLimits.setLimitsForX(-10.0, 10.0);
		graph.setGraphLimits(graphLimits);
		assertEquals(-10.0, graph.getGraphLimits().getMinX(), DELTA);
		assertEquals(10.0, graph.getGraphLimits().getMaxX(), DELTA);
	}

	@Test
	public void testGraphCheckForInvalidXAxis() {
		// Test setting invalid limits for x axis. Results in default values.
		graphLimits.setLimitsForX(10.0, -10.0);
		graph.setGraphLimits(graphLimits);
		assertEquals(-5.0, graph.getGraphLimits().getMinX(), DELTA);
		assertEquals(5.0, graph.getGraphLimits().getMaxX(), DELTA);
		// Test setting same limits for x axis. Results in default values.
		graphLimits.setLimitsForX(10.0, 10.0);
		graph.setGraphLimits(graphLimits);
		assertEquals(-5.0, graph.getGraphLimits().getMinX(), DELTA);
		assertEquals(5.0, graph.getGraphLimits().getMaxX(), DELTA);
	}

	@Test
	public void testGraphSetYAxis() {
		// Test setting all positive limits for y axis.
		graphLimits.setLimitsForY(10.0, 20.0);
		graph.setGraphLimits(graphLimits);
		assertEquals(10.0, graph.getGraphLimits().getMinY(), DELTA);
		assertEquals(20.0, graph.getGraphLimits().getMaxY(), DELTA);
		// Test setting limits on both sides of zero for y axis.
		graphLimits.setLimitsForY(-10.0, 10.0);
		graph.setGraphLimits(graphLimits);
		assertEquals(-10.0, graph.getGraphLimits().getMinY(), DELTA);
		assertEquals(10.0, graph.getGraphLimits().getMaxY(), DELTA);
	}

	@Test
	public void testGraphCheckForInvalidYAxis() {
		// Test setting invalid limits for y axis. Results in default values.
		graphLimits.setLimitsForY(10.0, -10.0);
		graph.setGraphLimits(graphLimits);
		assertEquals(-5.0, graph.getGraphLimits().getMinY(), DELTA);
		assertEquals(5.0, graph.getGraphLimits().getMaxY(), DELTA);
		// Test setting same limits for y axis. Results in default values.
		graphLimits.setLimitsForY(10.0, 10.0);
		graph.setGraphLimits(graphLimits);
		assertEquals(-5.0, graph.getGraphLimits().getMinY(), DELTA);
		assertEquals(5.0, graph.getGraphLimits().getMaxY(), DELTA);
	}

	@Test
	public void testGraphSetNumberCountForXAxis() {
		// Test setting different number of discrete points for x axis.
		graph.setNumberOfPointsForX(100);
		assertEquals(100, graph.getNumberOfPointsForX());
		graph.setNumberOfPointsForX(1);
		assertEquals(1, graph.getNumberOfPointsForX());
		graph.setNumberOfPointsForX(0);
		assertEquals(0, graph.getNumberOfPointsForX());
	}

	@Test
	public void testGraphSetInvalidNumberCountForXAxis() {
		// Test setting invalid number of discrete points for x axis.
		graph.setNumberOfPointsForX(-100);
		assertEquals(0, graph.getNumberOfPointsForX());
	}

	@Test
	public void testGraphSetNumberCountForYAxis() {
		// Test setting different number of discrete points for y axis.
		graph.setNumberOfPointsForY(100);
		assertEquals(100, graph.getNumberOfPointsForY());
		graph.setNumberOfPointsForY(1);
		assertEquals(1, graph.getNumberOfPointsForY());
		graph.setNumberOfPointsForY(0);
		assertEquals(0, graph.getNumberOfPointsForY());
	}

	@Test
	public void testGraphSetInvalidNumberCountForYAxis() {
		// Test setting invalid number of discrete points for y axis.
		graph.setNumberOfPointsForY(-100);
		assertEquals(0, graph.getNumberOfPointsForY());
	}

	@Test
	public void testGraphCheckForValidIncrementForXAxis() {
		// Test increment for x axis is calculated correctly.
		graphLimits.setLimitsForX(-10.0, 10.0);
		graph.setGraphLimits(graphLimits);
		graph.setNumberOfPointsForX(100);
		assertEquals(0.20202020202020202, graph.getIncrementForX(), DELTA);
		// Test increment for x axis is calculated correctly.
		graphLimits.setLimitsForX(-10.0, 20.0);
		graph.setGraphLimits(graphLimits);
		graph.setNumberOfPointsForX(200);
		assertEquals(0.1507537688442211, graph.getIncrementForX(), DELTA);
	}

	@Test
	public void testGraphCheckForValidIncrementForYAxis() {
		// Test increment for y axis is calculated correctly.
		graphLimits.setLimitsForY(-10.0, 10.0);
		graph.setGraphLimits(graphLimits);
		graph.setNumberOfPointsForY(100);
		assertEquals(0.20202020202020202, graph.getIncrementForY(), DELTA);
		// Test increment for y axis is calculated correctly.
		graphLimits.setLimitsForY(-10.0, 20.0);
		graph.setGraphLimits(graphLimits);
		graph.setNumberOfPointsForY(200);
		assertEquals(0.1507537688442211, graph.getIncrementForY(), DELTA);
	}

	@Test
	public void testCreateGraph() {
		// Test setters and getters for all fields.
		postfixSolver.parseExpression("x^2");
		graph.setExpressionSolver(postfixSolver);
		graphLimits.setLimitsForX(-15.0, 27.0);
		graphLimits.setLimitsForY(-7.0, 8.0);
		graph.setGraphLimits(graphLimits);
		graph.setNumberOfPointsForX(500);
		graph.setNumberOfPointsForY(400);
		assertEquals(postfixSolver, graph.getExpressionSolver());
		assertEquals(-15.0, graph.getGraphLimits().getMinX(), DELTA);
		assertEquals(27.0, graph.getGraphLimits().getMaxX(), DELTA);
		assertEquals(-7.0, graph.getGraphLimits().getMinY(), DELTA);
		assertEquals(8.0, graph.getGraphLimits().getMaxY(), DELTA);
		assertEquals(500, graph.getNumberOfPointsForX());
		assertEquals(400, graph.getNumberOfPointsForY());
		assertEquals(0.0841683366733467, graph.getIncrementForX(), DELTA);
		assertEquals(0.03759398496240601, graph.getIncrementForY(), DELTA);
	}

	@Test
	public void testGraphGetValuesExpressionIsNotSet() {
		// Test that graph is null if ExpressionSolver is not set.
		graphLimits.setLimitsForX(-15.0, 27.0);
		graphLimits.setLimitsForY(-7.0, 8.0);
		graph.setGraphLimits(graphLimits);
		graph.setNumberOfPointsForX(100);
		graph.setNumberOfPointsForY(100);
		graph.setGraph();
		assertNull(graph.getGraph());
	}

	@Test
	public void testGraphGetValuesExpressionIsInvalid() {
		// Test that graph is null if expression is not valid.
		try { postfixSolver.parseExpression("x+a"); } catch  (InvalidExpressionException exception) {}
		graph.setExpressionSolver(postfixSolver);
		graphLimits.setLimitsForX(-15.0, 27.0);
		graphLimits.setLimitsForY(-7.0, 8.0);
		graph.setGraphLimits(graphLimits);
		graph.setNumberOfPointsForX(100);
		graph.setNumberOfPointsForY(100);
		graph.setGraph();
		assertNull(graph.getGraph());
	}

	@Test
	public void testGraphGetValuesRangeIsInvalid() {
		postfixSolver.parseExpression("x+1");
		graph.setExpressionSolver(postfixSolver);
		graphLimits.setLimitsForX(-15.0, 27.0);
		graphLimits.setLimitsForY(-7.0, 8.0);
		graph.setGraphLimits(graphLimits);
		// Test that graph is null if x axis is not valid.
		graph.setNumberOfPointsForX(0);
		graph.setNumberOfPointsForY(100);
		graph.setGraph();
		assertNull(graph.getGraph());
		// Test that graph is null if y axis is not valid.
		graph.setNumberOfPointsForX(100);
		graph.setNumberOfPointsForY(0);
		graph.setGraph();
		assertNull(graph.getGraph());
		// Test that graph is not null if both axes are valid.
		graph.setNumberOfPointsForX(50);
		graph.setNumberOfPointsForY(50);
		graph.setGraph();
		assertNotNull(graph.getGraph());
	}

	@Test
	public void testGraphGetZeroIndexWithoutOrigo() {
		postfixSolver.parseExpression("x");
		graph.setExpressionSolver(postfixSolver);
		// Test that zeroIndex is 0 when x=0 is not within limits.
		graphLimits.setLimitsForX(5.0, 15.0);
		graph.setGraphLimits(graphLimits);
		graph.setNumberOfPointsForX(100);
		assertEquals(0, graph.getZeroIndex());
		// Test that zeroIndex is 0 when x=0 is not within limits.
		graphLimits.setLimitsForX(-15.0, -5.0);
		graph.setGraphLimits(graphLimits);
		graph.setNumberOfPointsForX(100);
		assertEquals(0, graph.getZeroIndex());
		// Test that zeroIndex is not 0 when x=0 is within limits.
		graphLimits.setLimitsForX(-5.0, 5.0);
		graph.setGraphLimits(graphLimits);
		graph.setNumberOfPointsForX(100);
		assertNotEquals(0, graph.getZeroIndex());
	}

	@Test
	public void testGraphGetZeroIndexWithOrigoAroundMiddle() {
		postfixSolver.parseExpression("x");
		graph.setExpressionSolver(postfixSolver);
		// Test that zeroIndex at middle when x=0 at middle of range.
		graphLimits.setLimitsForX(-5.0, 5.0);
		graph.setGraphLimits(graphLimits);
		graph.setNumberOfPointsForX(100);
		assertEquals(100 / 2, graph.getZeroIndex());
		// Test that specific zeroIndex value.
		graphLimits.setLimitsForX(-3.0, 2.0);
		graph.setGraphLimits(graphLimits);
		graph.setNumberOfPointsForX(100);
		assertEquals(59, graph.getZeroIndex());
	}

	@Test
	public void testGraphGetZeroIndexWithOrigoAtEndPosition() {
		postfixSolver.parseExpression("x");
		graph.setExpressionSolver(postfixSolver);
		// Test that zeroIndex is 0 x=0 is at left limit.
		graphLimits.setLimitsForX(0, 10.0);
		graph.setGraphLimits(graphLimits);
		graph.setNumberOfPointsForX(100);
		assertEquals(0, graph.getZeroIndex());
		// Test that zeroIndex is at last position when x=0 is at right limit.
		graphLimits.setLimitsForX(-10.0, 0);
		graph.setGraphLimits(graphLimits);
		graph.setNumberOfPointsForX(100);
		assertEquals(100 - 1, graph.getZeroIndex());
	}

	@Test
	public void testGraphGetValidValuesWithOrigo() {
		// Test a valid graph when origo is within limits.
		postfixSolver.parseExpression("x+1");
		graph.setExpressionSolver(postfixSolver);
		graphLimits.setLimitsForX(-15.0, 27.0);
		graphLimits.setLimitsForY(-7.0, 8.0);
		graph.setGraphLimits(graphLimits);
		graph.setNumberOfPointsForX(100);
		graph.setNumberOfPointsForY(50);
		graph.setGraph();
		ArrayList<GraphPoint> values = graph.getGraph();
		assertEquals(100, values.size());
		double incrementX = (27.0 - -15.0) / (100 - 1);
		double incrementY = (8.0 - -7.0) / (50 - 1);
		int expectedX, expectedY;
		int zeroIndex = graph.getZeroIndex();
		assertEquals(35, zeroIndex);
		for (int xCounter = 35;  xCounter < 100; xCounter++) {
//			System.out.format("Point x: %f, y: %f, xPixel: %d, yPixel: %d\n", values.get(xCounter).x, values.get(xCounter).y, values.get(xCounter).xPixel, values.get(xCounter).yPixel);
			assertEquals((xCounter - 35) * incrementX, values.get(xCounter).x, DELTA);
			assertEquals(1.0 + (xCounter - 35) * incrementX, values.get(xCounter).y, DELTA);
			expectedX = xCounter;
			expectedY = (int) Math.round(((1.0 + (xCounter - 35) * incrementX) - -7.0) / incrementY);
			assertEquals(expectedX, values.get(xCounter).xPixel);
			assertEquals(expectedY, values.get(xCounter).yPixel);
		}
		for (int xCounter = 34;  xCounter >= 0; xCounter--) {
//			System.out.format("Point x: %f, y: %f, xPixel: %d, yPixel: %d\n", values.get(xCounter).x, values.get(xCounter).y, values.get(xCounter).xPixel, values.get(xCounter).yPixel);
			assertEquals((xCounter - 35) * incrementX, values.get(xCounter).x, DELTA);
			assertEquals(1.0 + (xCounter - 35) * incrementX, values.get(xCounter).y, DELTA);
			expectedX = xCounter;
			expectedY = (int) Math.round(((1.0 + (xCounter - 35) * incrementX) - -7.0) / incrementY);
			assertEquals(expectedX, values.get(xCounter).xPixel);
			assertEquals(expectedY, values.get(xCounter).yPixel);
		}
	}

	@Test
	public void testGraphGetValidValuesWithoutOrigo() {
		// Test a valid graph when origo is not within limits.
		postfixSolver.parseExpression("x+1");
		graph.setExpressionSolver(postfixSolver);
		graphLimits.setLimitsForX(5.0, 15.0);
		graphLimits.setLimitsForY(5.0, 15.0);
		graph.setGraphLimits(graphLimits);
		graph.setNumberOfPointsForX(100);
		graph.setNumberOfPointsForY(100);
		graph.setGraph();
		ArrayList<GraphPoint> values = graph.getGraph();
		assertEquals(100, values.size());
		double incrementX = (15.0 - 5.0) / (100 - 1);
		double incrementY = (15.0 - 5.0) / (100 - 1);
		int expectedX, expectedY;
		int zeroIndex = graph.getZeroIndex();
		assertEquals(0, zeroIndex);
		for (int xCounter = 0;  xCounter < 100; xCounter++) {
//			System.out.format("Point x: %f, y: %f, xPixel: %d, yPixel: %d\n", values.get(xCounter).x, values.get(xCounter).y, values.get(xCounter).xPixel, values.get(xCounter).yPixel);
			assertEquals(5.0 + xCounter * incrementX, values.get(xCounter).x, DELTA);
			assertEquals(1.0 + 5.0 + xCounter * incrementX, values.get(xCounter).y, DELTA);
			expectedX = xCounter;
			expectedY = (int) Math.round(((1.0 + 5.0 + xCounter * incrementX) - 5.0) / incrementY);
			assertEquals(expectedX, values.get(xCounter).xPixel);
			assertEquals(expectedY, values.get(xCounter).yPixel);
		}
	}

	@Test
	public void testGraphGetValidValuesOrigoAtEndOfRange() {
		// Test a valid graph when origo is at one limit.
		postfixSolver.parseExpression("x+1");
		graph.setExpressionSolver(postfixSolver);
		graphLimits.setLimitsForX(-10.0, 0);
		graphLimits.setLimitsForY(-10.0, 0);
		graph.setGraphLimits(graphLimits);
		graph.setNumberOfPointsForX(100);
		graph.setNumberOfPointsForY(100);
		graph.setGraph();
		ArrayList<GraphPoint> values = graph.getGraph();
		assertEquals(100, values.size());
		double incrementX = (0 - -10.0) / (100 - 1);
		double incrementY = (0 - -10.0) / (100 - 1);
		int expectedX, expectedY;
		int zeroIndex = graph.getZeroIndex();
		assertEquals(99, zeroIndex);
		for (int xCounter = zeroIndex;  xCounter >= 0; xCounter--) {
//			System.out.format("Point x: %f, y: %f, xPixel: %d, yPixel: %d\n", values.get(xCounter).x, values.get(xCounter).y, values.get(xCounter).xPixel, values.get(xCounter).yPixel);
			assertEquals(0 - (zeroIndex - xCounter) * incrementX, values.get(xCounter).x, DELTA);
			assertEquals(1.0 - (zeroIndex - xCounter) * incrementX, values.get(xCounter).y, DELTA);
			expectedX = xCounter;
			expectedY = (int) Math.round(((1.0 - (zeroIndex - xCounter) * incrementX) - -10.0) / incrementY);
			assertEquals(expectedX, values.get(xCounter).xPixel);
			assertEquals(expectedY, values.get(xCounter).yPixel);
		}
	}

	@Test
	public void testGraphFindXWhenGraphInvalid() {
		//
		postfixSolver.parseExpression("x+1");
		graph.setExpressionSolver(postfixSolver);
		int index = graph.getIndexCloseToX(1.0);
		assertEquals(-1, index);
	}

	@Test
	public void testGraphFindXWhenGraphValid() {
		//
		postfixSolver.parseExpression("x+1");
		graph.setExpressionSolver(postfixSolver);
		graphLimits.setLimitsForX(-5.0, 5.0);
		graphLimits.setLimitsForY(-5.0, 5.0);
		graph.setGraphLimits(graphLimits);
		graph.setNumberOfPointsForX(200);
		graph.setNumberOfPointsForY(200);
		graph.setGraph();
		double increment = (5.0 - -5.0) / (200 - 1);
		//
		int index = graph.getIndexCloseToX(0);
		/*System.out.println("index: " + index +
				   ", x: " + graph.getGraph().get(index).x +
				   ", y: " + graph.getGraph().get(index).y);*/
		assertEquals(100, index);
		assertEquals(0, graph.getGraph().get(index).x, increment / 2);
		//
		index = graph.getIndexCloseToX(1);
		/*System.out.println("index: " + index +
				   ", x: " + graph.getGraph().get(index).x +
				   ", y: " + graph.getGraph().get(index).y);*/
		assertEquals(120, index);
		assertEquals(1, graph.getGraph().get(index).x, increment / 2);
		//
		index = graph.getIndexCloseToX(3.456);
		/*System.out.println("index: " + index +
				   ", x: " + graph.getGraph().get(index).x +
				   ", y: " + graph.getGraph().get(index).y);*/
		assertEquals(169, index);
		assertEquals(3.456, graph.getGraph().get(index).x, increment / 2);
		//
		index = graph.getIndexCloseToX(-4.87873);
		/*System.out.println("index: " + index +
				   ", x: " + graph.getGraph().get(index).x +
				   ", y: " + graph.getGraph().get(index).y);*/
		assertEquals(3, index);
		assertEquals(-4.87873, graph.getGraph().get(index).x, increment / 2);
	}

	@Test
	public void testGraphEvaluationError() {
		postfixSolver.parseExpression("2x");
		graph.setExpressionSolver(postfixSolver);
		graphLimits.setLimitsForX(-5.0, 5.0);
		graphLimits.setLimitsForY(-5.0, 5.0);
		graph.setGraphLimits(graphLimits);
		graph.setNumberOfPointsForX(200);
		graph.setNumberOfPointsForY(200);
		try {
			graph.setGraph();
			fail("Exception expected");
		} catch (EvaluationErrorException e) {
			assertEquals("Could not evaluate expression", postfixSolver.getMessage());
			assertNull(graph.getGraph());
		}
	}
}

/* Graph.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator.graph;

import java.util.ArrayList;

import fi.markkola.calculator.expression.EvaluationErrorException;
import fi.markkola.calculator.expression.ExpressionSolver;

/**
 * This class represents a graph on a Cartesian coordinate system, with x and y
 * axes, that is mapped to a graphical presentation on a canvas. Each point on
 * the graph has values for Cartesian coordinates as well for discrete point
 * for a graphical presentation on a canvas.
 * <p>
 * The graph is formed by using provided instance of an ExpressionSolver
 * class. This instance calculates values of y for given x. Values for y are
 * calculated only for x that fall within the limits for x that must be set
 * before calculating the graph.
 * <p>
 * Graphical representation for each point is determined so that 0,0 is on the
 * bottom left corner.
 *
 * @author Paavo Markkola
 */
public class Graph {
	/**
	 * ExpressionSolver instance that is used to calculate values for each
	 * point on the graph. It is assumed that the expression has already been
	 * parsed.
	 */
	private ExpressionSolver expressionSolver;
	/**
	 * A list of points that form the graph. Each point has Cartesian coordinate
	 * values values as well their representation in graphical system.
	 */
	private ArrayList<GraphPoint> graph;
	/**
	 * Minimum and maximum limits on x and y axes for the graph.
	 */
	private GraphLimits graphLimits;
	/**
	 * The number of discrete points on x and y axes.
	 */
	private int discretePointsOnXAxis = 0;
	private int discretePointsOnYAxis = 0;
	/**
	 * The increment in real value for each discrete point on x and y axes.
	 */
	private double incrementForX = 0;
	private double incrementForY = 0;
	/**
	 * Index of the graph element on x-axis where x has the value of 0.
	 */
	private int zeroIndex;
	/**
	 * Construct new Graph instance with default range and empty graph.
	 */
	public Graph() {
		setExpressionSolver(null);
		clearGraph();
		clearRange();
	}
	/**
	 * Empty current graph and set it to null.
	 */
	private void clearGraph() {
		if (graph != null)
			graph.clear();
		graph = null;
	}
	/**
	 * Set default values to limits, number of points and increments both axes.
	 * Also, restore zeroIndex to 0.
	 */
	private void clearRange() {
		if (graphLimits == null)
			graphLimits = new GraphLimits();
		else
			graphLimits.setDefaultLimits();
		discretePointsOnXAxis = 0;
		incrementForX = 0;
		discretePointsOnYAxis = 0;
		incrementForY = 0;
		zeroIndex = 0;
	}
	/**
	 * Set ExpressionSolver instance for the graph. The ExpressionSolver is
	 * used to calculate the graph values for y-axis.
	 *
	 * @param solver	The ExpressionSolver instance to be used for the graph.
	 */
	public void setExpressionSolver(ExpressionSolver solver) {
		expressionSolver = solver;
	}
	/**
	 * Return ExpressionSolver instance for the graph.
	 *
	 * @return	The ExpressionSolver used for the graph
	 */
	public ExpressionSolver getExpressionSolver() {
		return expressionSolver;
	}
	/**
	 * Calculates new graph. For the graph to be calculated configured
	 * expression must be valid, limits must have been set properly and number
	 * of discrete point on each axis must be valid as well.
	 * <p>
	 * Sets graph to null if any of the requirements are not valid.
	 *
	 * @throws EvaluationErrorException When expression cannot be evaluated.
	 */
	public void setGraph() {
		if (!isValidExpression())
			clearGraph();
		else if (!isValidRange())
			clearGraph();
		else
			try {
				setGraphPoints();
			} catch (EvaluationErrorException exception) {
				clearGraph();
				throw exception;
			}
	}

	/**
	 * Returns data structure containing all points in the graph within range.
	 *
	 * @return Linked list containing all points in the graph within set range.
	 */
	public ArrayList<GraphPoint> getGraph() {
		return graph;
	}
	/**
	 * Verify that the expressionSolver has been configured and that the
	 * expression has been parsed successfully by the expressionSolver.
	 *
	 * @return	True if the expression was parsed successfully, false if not.
	 */
	private boolean isValidExpression() {
		if ((expressionSolver != null) &&
			(expressionSolver.getStatus()))
			return true;
		return false;
	}
	/**
	 * Verify that configured values for the graph's range are valid. The Range
	 * is valid if the minimum value is less than the maximum value on both y
	 * and x axes. Also, the number of discrete points on both axes must be
	 * greater than zero. Thus the increment for each discrete point on both
	 * axes must be greater than zero.
	 *
	 * @return	True if range is valid, false if not.
	 */
	private boolean isValidRange() {
		if (graphLimits == null)
			return false;
		if ((graphLimits.getMinX() < graphLimits.getMaxX()) &&
			(graphLimits.getMinY() < graphLimits.getMaxY()) &&
			(discretePointsOnXAxis > 0) && (incrementForX > 0) &&
			(discretePointsOnYAxis > 0) && (incrementForY > 0))
			return true;
		return false;
	}
	/**
	 * Fill graph data structure with elements for each discrete point on the
	 * x-axis. That is, determine value for x on each point on the x-axis and
	 * add it the list.
	 * <p>
	 * If x is zero within the range for x-axis then start from the point
	 * corresponding to the point where x is zero on the x-axis. If x is not
	 * zero on either end of the range then proceed in both directions
	 * separately. If x is not zero within range then start from the first
	 * point on the x-axis.
	 */
	private void setGraphPoints() {
		double offsetValue = 0;
		initGraphPoints();
		if (zeroIndex == 0)
			offsetValue = graphLimits.getMinX();
		for (int xIndex = zeroIndex; xIndex < discretePointsOnXAxis; xIndex++)
			addGraphPoint(xIndex, offsetValue +
							(xIndex - zeroIndex) * incrementForX);
		for (int xIndex = zeroIndex - 1; xIndex >=  0; xIndex--)
			addGraphPoint(xIndex, offsetValue -
							(zeroIndex - xIndex) * incrementForX);
	}
	/**
	 * Initialize graph data structure. Set the size of the structure to be
	 * the amount of discrete points on the x-axis. Set each element to zero
	 * to make sure that size() method will return the correct capacity of the
	 * data structure.
	 */
	private void initGraphPoints() {
		graph = new ArrayList<>(discretePointsOnXAxis);
		for (int i = 0; i < discretePointsOnXAxis; i++)
			graph.add(i, null);
	}

	/**
	 * Adds single point with given value for x to the graph data structure.
	 * Calculates the value of y using the the value of x from the equation,
	 * as well the discrete point of y on the y-axis.
	 *
	 * @param xIndex	Index of the discrete point on the x-axis
	 * @param xValue	Value of x for the point on the x-axis
	 */
	private void addGraphPoint(int xIndex, double xValue) {
		double yValue;
		int yIndex;
		yValue = expressionSolver.evaluateExpression(xValue);
		if (Double.isNaN(yValue))
			yIndex = -1;
		else
			yIndex = (int) Math.round((yValue - graphLimits.getMinY()) /
															incrementForY);
		graph.set(xIndex, new GraphPoint(xValue, yValue, xIndex, yIndex));
	}
	/**
	 * Set GraphLimits instance for the graph.
	 * <p>
	 * If number of discrete points on x/y-axis is valid as well then set
	 * increment for each point on the axes as well. Also, find the index of
	 * the point on the x-axis where x is 0.
	 *
	 * @param graphLimits	GraphLimits instance to be set for the graph.
	 */
	public void setGraphLimits(GraphLimits graphLimits) {
		this.graphLimits = graphLimits;
		setIncrementForX();
		setIncrementForY();
		setZeroIndex();
	}
	/**
	 * Return GraphLimits instance configured for the graph.
	 *
	 * @return	GraphLimits instance used for the graph.
	 */
	public GraphLimits getGraphLimits() {
		return graphLimits;
	}
	/**
	 * Set the number of discrete points on x-axis. The value set must be
	 * higher than zero.
	 * <p>
	 * If configured range for x-axis is also valid then set value increment
	 * for x on each discrete point on the x-axis as well. Also, find the index
	 * of the point on the x-axis where x is zero.
	 *
	 * @param numberOfPoints	The number of discrete points on the x-axis
	 */
	public void setNumberOfPointsForX(int numberOfPoints) {
		if (numberOfPoints < 1)
			discretePointsOnXAxis = 0;
		else
			discretePointsOnXAxis = numberOfPoints;
		setIncrementForX();
		setZeroIndex();
	}
	/**
	 * Return the number of discrete points currently set for the x-axis.
	 *
	 * @return The number of discrete points on the x-axis
	 */
	public int getNumberOfPointsForX() {
		return discretePointsOnXAxis;
	}
	/**
	 * Set the increment for value of x for each discrete point on the x-axis.
	 * The value of x is added or subtracted by the increment value when moving
	 * on the x-axis.
	 * <p>
	 * The increment is calculated by dividing range difference for x by the
	 * amount of discrete points on the x-axis.
	 */
	private void setIncrementForX() {
		if ((discretePointsOnXAxis > 1) &&
			(graphLimits.getMaxX() > graphLimits.getMinX()))
			incrementForX = (graphLimits.getMaxX() -
								 graphLimits.getMinX()) /
								(discretePointsOnXAxis - 1);
		else
			incrementForX = 0;
	}

	/**
	 * Return the increment for value of x for each discrete point on the
	 * x-axis.
	 *
	 * @return	The increment for value of x for each discrete point on the
	 * 			x-axis
	 */
	public double getIncrementForX() {
		return incrementForX;
	}
	/**
	 * Set number of points on y-axis. The number of points must be higher than
	 * zero.
	 * <p>
	 * If valid range is set for y-axis then set increment for each point on
	 * the y-axis as well.
	 *
	 * @param numberOfPoints	The number of points on the y-axis
	 */
	public void setNumberOfPointsForY(int numberOfPoints) {
		if (numberOfPoints < 1)
			discretePointsOnYAxis = 0;
		else
			discretePointsOnYAxis = numberOfPoints;
		setIncrementForY();
	}

	/**
	 * Return the number of discrete points currently set for the y-axis.
	 *
	 * @return The number of discrete points on the y-axis
	 */
	public int getNumberOfPointsForY() {
		return discretePointsOnYAxis;
	}
	/**
	 * Set the increment for value of y for each discrete point on the y-axis.
	 * The value of y is added or subtracted by the increment value when moving
	 * on the y-axis.
	 * <p>
	 * The increment is calculated by dividing range difference for y by the
	 * amount of discrete points on the y-axis.
	 */
	private void setIncrementForY() {
		if ((discretePointsOnYAxis > 0) &&
			(graphLimits.getMaxY() > graphLimits.getMinY()))
			incrementForY = (graphLimits.getMaxY() -
								 graphLimits.getMinY()) /
								(discretePointsOnYAxis - 1);
		else
			incrementForY = 0;
	}
	/**
	 * Return the increment for value of y for each discrete point on the
	 * y-axis.
	 *
	 * @return	The increment for value of y for each discrete point on the
	 * 			y-axis
	 */
	public double getIncrementForY() {
		return incrementForY;
	}
	/**
	 * Finds the index of the element on x-axis where x is zero and stores the
	 * value to the field zeroIndex. Calculation for graph values starts from
	 * this index, in both directions if necessary.
	 */
	private void setZeroIndex() {
		// if x is either zero on the right end of the range, or if x is not
		// zero on the range.
		if ((graphLimits.getMinX() >= 0) || (graphLimits.getMaxX() < 0))
			zeroIndex = 0;
		// x is zero on left end of the range
		else if ((graphLimits.getMinX() < 0) && (graphLimits.getMaxX() == 0))
			zeroIndex = discretePointsOnXAxis - 1;
		// Check for error case...
		else if (graphLimits.getMaxX() - graphLimits.getMinX() == 0)
			zeroIndex = 0;
		// Calculate zero index
		else
			zeroIndex = (int) Math.round((discretePointsOnXAxis - 1) *
							  Math.abs(graphLimits.getMinX()) /
							  (graphLimits.getMaxX() - graphLimits.getMinX()));
	}
	/**
	 * Returns the index of the graph element on x-axis where x has the value
	 * of 0. The index depends on the range of values and on the number of
	 * discrete points on the x-axis. Both must be set properly in order to
	 * get a valid index. If x is not zero within the range, then the index is
	 * set to zero.
	 * <p>
	 * Calculating the graph always starts from the zero index, in both
	 * directions if necessary. This is to ensure that the value for y is
	 * always accurate when x is zero.
	 * <p>
	 * The index is set to maximum if x is zero on the right end of the range
	 * on x-axis, and to minimum if x is not zero on the range or if x is zero
	 * on the left end of the range. In all other cases the index is set to the
	 * element where x is zero.
	 *
	 * @return Index of the element on x-axis where x = 0.
	 */
	public int getZeroIndex() {
		return zeroIndex;
	}
	/**
	 * Finds the index of the point on the graph with the value for x closest
	 * to the value given as parameter.
	 * <p>
	 * Returns -1 if graph is not defined or if a point with close enough value
	 * for x cannot be found.
	 *
	 * @param value	Value for x to searched from the graph.
	 * @return		Index of the found point on the graph.
	 */
	public int getIndexCloseToX(double value) {
		if (graph == null)
			return -1;
		for (int index = 0; index < graph.size(); index++)
			if (((graph.get(index).x + incrementForX / 2) >= value) &&
			    ((graph.get(index).x - incrementForX / 2) <= value))
				return index;
		return -1;
	}
}

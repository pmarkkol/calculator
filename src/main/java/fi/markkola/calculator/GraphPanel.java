/* GraphPanel.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Locale;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;

import fi.markkola.calculator.expression.EvaluationErrorException;
import fi.markkola.calculator.expression.PostfixSolver;
import fi.markkola.calculator.graph.Graph;
import fi.markkola.calculator.graph.GraphLimits;
import fi.markkola.calculator.graph.GraphLine;
import fi.markkola.calculator.graph.GraphPoint;

/**
 * JPanel that is used to display coordinates and graphs.
 *
 * @author Paavo Markkola
 */
@SuppressWarnings("serial")
public class GraphPanel extends JPanel {
	/**
	 * Default spacing between each cross line in pixels.
	 */
	private static final int DEFAULT_CROSS_LINE_SPACING = 50;
	/**
	 * Default number of cross lines. Should be even so that both sides of
	 * the origo line have the same number of cross lines.
	 */
	private static final int DEFAULT_CROSS_LINES_COUNT = 8;
	/**
	 * The graph as mathematical expression as provided by the user. Must be
	 * set and be valid for the graph to be drawn.
	 */
	private String expression;
	/**
	 * PostfixSolver instance that is used to calculate values for each point
	 * on the graph.
	 */
	private PostfixSolver postfixSolver;
	/**
	 * The class that handles building of the graph from the expression string.
	 */
	private Graph graph;
	/**
	 * Minimum and maximum limits on x and y axes for the graph.
	 */
	private GraphLimits graphLimits;
	/**
	 * When cursor is enable this point contains the location of the cursor.
	 */
	private GraphPoint cursorPoint;
	/**
	 * Vertical and horizontal lines that cross at the origo. Set to null if
	 * the line is included with currently used graph limits.
	 */
	private GraphLine verticalOrigoLine, horizontalOrigoLine;
	/**
	 * Index of the graph where cursor is currently located.
	 */
	private int cursorIndex;
	/**
	 * Boolean indicating whether cursor is to be displayed or not.
	 */
	private boolean cursorShow = false;
	/**
	 * Boolean indicating whether the graph needs to be recalculated or not.
	 * Recalculation is needed for example when limits are changed.
	 */
	private boolean graphUpdateNeeded;
	/**
	 * Boolean indicating whether the cursor position needs to be updated or
	 * not. Set for example when zooming in/out.
	 */
	private boolean cursorUpdateNeeded;
	/**
	 * Create new GraphPanel instance with default coordinates and no graph.
	 */
	public GraphPanel() {
		initGraphPanel();
	}
	/**
	 * Initialize graph panel with default values.
	 */
	private void initGraphPanel() {
		setExpression(null);
		setPostfixCalculator();
		setGraphLimits();
		setGraph();
		setGraphPoints();
		resetOrigoLines();
		resetCursor();
	}
	/**
	 * Set the string given as parameter as the expression for the graph.
	 *
	 * @param expression	Mathematical expression for the graph as a string.
	 */
	private void setExpression(String expression) {
		this.expression = expression;
	}
	/**
	 * Parse expression string, if it has been set and is not an empty string,
	 * using configured PostfixSolver instance. Create new PostfixSolver
	 * instance in case it does not yet exist.
	 */
	private void setPostfixCalculator() {
		if (postfixSolver == null)
			postfixSolver = new PostfixSolver();
		if (expression != null && !expression.isEmpty())
			postfixSolver.parseExpression(expression);
	}
	/**
	 * Set default limits for the graph. Create new instance of GraphLimits if
	 * graph limits have not yet been initialized.
	 */
	private void setGraphLimits() {
		if (graphLimits == null)
			graphLimits = new GraphLimits();
		else
			setDefaultLimits();
	}
	/**
	 * Configure a new instance of Graph class. Set the new graph to be
	 * calculated at next call to paintComponet method. Graph is set to null
	 * if expression string has not been set or is empty.
	 */
	private void setGraph() {
		if (expression == null || expression.isEmpty())
			graph = null;
		else {
			graph = new Graph();
			graph.setExpressionSolver(postfixSolver);
			setGraphDimensions();
			setGraphUpdate(true);
		}
	}
	/**
	 * Set dimensions for the Graph object. This includes limits and number of
	 * discrete points on x and y axes.
	 */
	private void setGraphDimensions() {
		if ((graph != null) && (graphLimits != null)) {
			graph.setGraphLimits(graphLimits);
			graph.setNumberOfPointsForX(getWidth());
			graph.setNumberOfPointsForY(getHeight());
		}
	}
	/**
	 * Update the list of coordinate points representing a graph. New value
	 * for each point is calculated only if update was requested earlier.
	 */
	private void setGraphPoints() {
		if (graph != null) {
			if (graphUpdateNeeded)
				try {
					graph.setGraph();
				} catch  (EvaluationErrorException exception) {
					JFrame frame =
								(JFrame) SwingUtilities.getWindowAncestor(this);
					JOptionPane.showMessageDialog(frame,
												  exception.getMessage(),
												  "Error in evaluation",
							  					  JOptionPane.ERROR_MESSAGE);
				}
			setGraphUpdate(false);
		}
	}
	/**
	 * Set whether graph should be updated and recalculated at next call to
	 * paintComponent.
	 *
	 * @param update	True if the graph needs an update, false otherwise
	 */
	private void setGraphUpdate(boolean update) {
		graphUpdateNeeded = update;
	}
	/**
	 * Restores coordinate limits back to default values for both x and y axes.
	 */
	private void setDefaultLimits() {
		graphLimits.setDefaultLimits();
	}
	/**
	 * Create new GraphLine instances for origo lines at correct positions. In
	 * case graph does not exist yet set the origo lines to dead center of the
	 * canvas. Otherwise, calculate positions for the origo lines.
	 */
	private void setOrigoLines() {
		if (graph == null)
			resetOrigoLines();
		else {
			setVerticalOrigoLine();
			setHorizontalOrigoLine();
		}
	}
	/**
	 * Set vertical origo line. If x = 0 is not on within limits then set the
	 * line to null. Otherwise calculate vertical position of the line and
	 * create new GraphLine object for the position.
	 */
	private void setVerticalOrigoLine() {
		int x = (int) Math.round((Math.abs(graphLimits.getMinX() /
				(graphLimits.getMinX() - graphLimits.getMaxX()))) * getWidth());
		if ((graphLimits.getMinX() <= 0) && (graphLimits.getMaxX() >= 0 ))
			verticalOrigoLine = new GraphLine(x, 0, x, getHeight());
		else
			verticalOrigoLine = null;
	}
	/**
	 * Set horizontal origo line. If y = 0 is not on within limits then set the
	 * line to null. Otherwise calculate horizontal position of the line and
	 * create new GraphLine object for the position.
	 */
	private void setHorizontalOrigoLine() {
		int y = (int) Math.round((Math.abs(graphLimits.getMaxY() /
			   (graphLimits.getMinY() - graphLimits.getMaxY()))) * getHeight());
		if ((graphLimits.getMinY() <= 0) && (graphLimits.getMaxY() >= 0 ))
			horizontalOrigoLine = new GraphLine(0, y, getWidth(), y);
		else
			horizontalOrigoLine = null;
	}
	/**
	 * Get position of the vertical origo line. If the line does not exist
	 * then return vertical center of the canvas.
	 *
	 * @return	Position of the vertical origo line.
	 */
	private int getVerticalOrigoLinePosition() {
		if (verticalOrigoLine != null)
			return verticalOrigoLine.x1;
		else
			return getWidth() / 2;
	}
	/**
	 * Get position of the horizontal origo line. If the line does not exist
	 * then return horizontal center of the canvas.
	 *
	 * @return	Position of the horizontal origo line.
	 */
	private int getHorizontalOrigoLinePosition() {
		if (horizontalOrigoLine != null)
			return horizontalOrigoLine.y1;
		else
			return getHeight() / 2;
	}
	/**
	 * Reset origo lines back to dead center of the canvas.
	 */
	private void resetOrigoLines() {
		verticalOrigoLine = new GraphLine(getWidth() / 2, 0,
										  getWidth() / 2, getHeight());
		horizontalOrigoLine = new GraphLine(0, 			getHeight() / 2,
									        getWidth(), getHeight() / 2);
	}
	/**
	 * Determine if the point on a graph at given index is a valid cursor
	 * point. For the point to be a valid cursor point, the graph must have
	 * been properly set, the index must fall within the graph and the point
	 * must be visible.
	 *
	 * @param index		Index of the point on a graph to be checked.
	 */
	private boolean isValidCursorIndex(int index) {
		if ((graph == null) || (graph.getGraph() == null))
			return false;
		if ((index < 0) || (index >= graph.getGraph().size()))
			return false;
		GraphPoint point = graph.getGraph().get(index);
		if ((point.xPixel < 0) || (point.yPixel < 0) ||
			(point.xPixel > getWidth()) || (point.yPixel > getHeight()))
			return false;
		return true;
	}
	/**
	 * Set cursor point if the graph is properly set.
	 */
	private void setCursorPoint() {
		if ((graph == null) || (graph.getGraph() == null)) {
			resetCursor();
			return;
		}
		if ((cursorUpdateNeeded) && (cursorPoint != null))
			cursorIndex = graph.getIndexCloseToX(cursorPoint.x);
		if (cursorIndex == -1)
			cursorIndex = graph.getZeroIndex();
		if (isValidCursorIndex(cursorIndex))
			cursorPoint = graph.getGraph().get(cursorIndex);
		else
			cursorPoint = null;
		setCursorUpdate(false);
	}
	/**
	 * Show or hide cursor.
	 *
	 * @param show	True if cursor is to be shown and false if not
	 */
	private void setShowCursor(boolean show) {
		cursorShow = show;
	}
	/**
	 * Reset cursor position.
	 */
	private void resetCursor() {
		setCursorUpdate(false);
		cursorIndex = -1;
		cursorPoint = null;
	}
	/**
	 * Set whether cursor should be updated and recalculated at next call to
	 * paintComponent.
	 *
	 * @param update	True if the cursor needs an update, false otherwise
	 */
	private void setCursorUpdate(boolean update) {
		cursorUpdateNeeded = update;
	}
	/**
	 * Update limits for the graph on both axes.
	 *
	 * @param minX	Minimum limit for x.
	 * @param maxX	Maximum limit for x.
	 * @param minY	Minimum limit for y.
	 * @param maxY	Maximum limit for y.
	 */
	private void updateLimits(double minX, double maxX,
							  double minY, double maxY) {
		graphLimits.setLimitsForX(minX, maxX);
		graphLimits.setLimitsForY(minY, maxY);
	}
	/**
	 * Override paintComponent method of the JPanel component.
	 */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		super.paintBorder(g);
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setBackground(Color.WHITE);
		setGraphDimensions();
		setGraphPoints();
		setOrigoLines();
		setCursorPoint();
		drawCoordinates(g);
		drawGraph(g, Color.blue);
		drawCursor(g);
	}
	/**
	 * Draw the actual graph. Use to the color provided for the graph. Display
	 * expression string at the top left corner for reference.
	 *
	 * @param g		Graphics object to which the graph is to be drawn.
	 * @param c		Color for the graph.
	 */
	private void drawGraph(Graphics g, Color c) {
		Color originalColor = g.getColor();
		g.setColor(c);
		if (expression != null)
			g.drawString(expression, 10, 10);
		if ((graph != null) && (graph.getGraph() != null)) {
			for (int index = 0; index < graph.getGraph().size(); index++) {
				if (index + 1 >= graph.getGraph().size())
					break;
				if ((isVisible(graph.getGraph().get(index))) &&
					(isVisible(graph.getGraph().get(index + 1))))
					drawGraphLine(g, graph.getGraph().get(index),
									 graph.getGraph().get(index + 1));
			}
		}
		g.setColor(originalColor);
	}
	/**
	 * Check if GraphPoint is visible on panel. To be visible location of the
	 * point must be within size limits of the panel.
	 *
	 * @param point		GraphPoint to be checked for visibility.
	 * @return			True if visible and false if not.
	 */
	private boolean isVisible(GraphPoint point) {
		if (point == null)
			return false;
		if ((point.xPixel >= 0) && (point.xPixel <= getWidth()) &&
			(point.yPixel >= 0) && (point.yPixel <= getHeight()))
			return true;
		return false;
	}
	/**
	 * Draw a line between two points on a graph.
	 *
	 * @param g		Graphics object to which the line is to be drawn.
	 * @param start	Start point of the line.
	 * @param end	End point of the line.
	 */
	private void drawGraphLine(Graphics g, GraphPoint start, GraphPoint end) {
		g.drawLine(start.xPixel, Math.abs(start.yPixel - getHeight()),
				   end.xPixel,   Math.abs(end.yPixel - getHeight()));
	}
	/**
	 * Draw cursor lines. Nothing is drawn if the cursor is not set to visible
	 * or if the cursor point is not set.
	 *
	 * @param g	Graphics object to which the cursor lines are to be drawn.
	 */
	private void drawCursor(Graphics g) {
		if ((!cursorShow) || (cursorPoint == null))
			return;
		Graphics2D g2d = (Graphics2D) g.create();
		setDashedStroke(g2d);
		g2d.setColor(Color.red);
		drawCursorLines(g2d, cursorPoint);
		drawPointValues(g, cursorPoint);
		g2d.dispose();
	}
	/**
	 * Set dashed stroke using Graphics2D.
	 *
	 * @param g2d	Graphics2D object to which the stroke is to be set.
	 */
	public void setDashedStroke(Graphics2D g2d) {
		g2d.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT,
									  BasicStroke.JOIN_BEVEL, 0,
									  new float[]{2}, 0));
	}
	/**
	 * Draw cursor lines at specified point.
	 *
	 * @param g2d		Graphics2D object to which the lines are to be drawn.
	 * @param point		The graph point at which the lines are drawn.
	 */
	private void drawCursorLines(Graphics2D g2d, GraphPoint point) {
		g2d.drawLine(point.xPixel,	0,
					 point.xPixel,	getHeight());
		g2d.drawLine(0, 		 	Math.abs(point.yPixel - getHeight()),
					 getWidth(), 	Math.abs(point.yPixel - getHeight()));
	}
	/**
	 * Draw string indicating x,y values of a point.
	 *
	 * @param g		Graphics object to which the string is to be drawn.
	 * @param point	The graph point at which the string is to be drawn.
	 */
	private void drawPointValues(Graphics g, GraphPoint point) {
		String location = String.format(Locale.ROOT, "(%.2f:%.2f)",
										cursorPoint.x, cursorPoint.y);
		g.drawString(location, point.xPixel + 10,
							   Math.abs(point.yPixel - getHeight()) + 10);
	}

	/**
	 * Draw coordinates. This includes origo lines, cross lines and limits on
	 * both axes.
	 *
	 * @param g		Graphics object to which the coordinates are to be drawn.
	 */
	private void drawCoordinates(Graphics g) {
		Color originalColor = g.getColor();
		g.setColor(Color.lightGray);
		drawCrossLines(g);
		g.setColor(Color.black);
		drawOrigoLines(g);
		drawLimits(g);
		g.setColor(originalColor);
	}
	/**
	 * Draw vertical and horizontal cross lines.
	 *
	 * @param g		Graphics object to which the lines are to be drawn.
	 */
	private void drawCrossLines(Graphics g) {
		Graphics2D g2d = (Graphics2D) g.create();
		setDashedStroke(g2d);
		drawVerticalCrossLines(g2d, getVerticalCrossLineSpacing());
		drawHorizontalCrossLines(g2d, getHorizontalCrossLineSpacing());
		g2d.dispose();
	}
	/**
	 * Draw vertical cross lines. Start from vertical origo line if it exist
	 * and proceed in both directions separately if necessary.
	 *
	 * @param g2d		Graphics object to which the lines are to be drawn.
	 * @param xSpacing	Spacing in between each cross line in pixels.
	 */
	private void drawVerticalCrossLines(Graphics2D g2d, int xSpacing) {
		for (int x = getVerticalOrigoLinePosition(); x < getWidth(); x += xSpacing)
			g2d.drawLine(x, 0, x, getHeight());
		for (int x = getVerticalOrigoLinePosition(); x > 0; x -= xSpacing)
			g2d.drawLine(x, 0, x, getHeight());
	}
	/**
	 * Draw horizontal cross lines. Start from horizontal origo line if it
	 * exist and proceed in both directions separately if necessary.
	 *
	 * @param g2d		Graphics object to which the lines are to be drawn.
	 * @param xSpacing	Spacing in between each cross line in pixels.
	 */
	private void drawHorizontalCrossLines(Graphics2D g2d, int ySpacing) {
		for (int y = getHorizontalOrigoLinePosition(); y < getHeight(); y += ySpacing)
			g2d.drawLine(0, y, getWidth(), y);
		for (int y = getHorizontalOrigoLinePosition(); y > 0; y -= ySpacing)
			g2d.drawLine(0, y, getWidth(), y);
	}
	/**
	 * Get spacing between vertical cross lines.
	 *
	 * @return	The number of pixels between vertical cross lines.
	 */
	private int getVerticalCrossLineSpacing() {
		return getWidth() / getNumberOfCrossLines();
	}
	/**
	 * Get spacing between horizontal cross lines.
	 *
	 * @return	The number of pixels between horizontal cross lines.
	 */
	private int getHorizontalCrossLineSpacing() {
		return getHeight() / getNumberOfCrossLines();
	}
	/**
	 * Get number of cross lines. Use constant DEFAULT_NUMBER_OF_CROSS_LINES as
	 * initial number of lines. Add or subtract lines if spacing is less then
	 * or more than DEFAULT_CROSS_LINE_SPACING.
	 *
	 * @return	The number of cross lines.
	 */
	private int getNumberOfCrossLines() {
		int numberOfLines = DEFAULT_CROSS_LINES_COUNT;
		if ((getWidth() / numberOfLines) < DEFAULT_CROSS_LINE_SPACING)
			while ((getWidth() / numberOfLines) < DEFAULT_CROSS_LINE_SPACING)
				if (numberOfLines > 2)
					numberOfLines -= 2;
				else
					break;
		else
			while ((getWidth() / numberOfLines) > DEFAULT_CROSS_LINE_SPACING)
				numberOfLines += 2;
		return numberOfLines;
	}
	/**
	 * Draw vertical and horizontal origo lines.
	 *
	 * @param g		Graphics object to which the lines are to be drawn.
	 */
	private void drawOrigoLines(Graphics g) {
		 if (verticalOrigoLine != null)
			 g.drawLine(verticalOrigoLine.x1, verticalOrigoLine.y1,
					    verticalOrigoLine.x2, verticalOrigoLine.y2);
		 if (horizontalOrigoLine != null)
			 g.drawLine(horizontalOrigoLine.x1, horizontalOrigoLine.y1,
					 	horizontalOrigoLine.x2, horizontalOrigoLine.y2);
	}
	/**
	 * Draw limits of both axes on the canvas.
	 *
	 * @param g		Graphics object to which the limits are to be drawn.
	 */
	private void drawLimits(Graphics g) {
		g.drawString(String.format(Locale.ROOT, "%.2f", graphLimits.getMinX()),
								   0, getHeight() / 2 - 5);
		g.drawString(String.format(Locale.ROOT, "%.2f", graphLimits.getMaxX()),
								   getWidth() - 30, getHeight() / 2 - 5);
		g.drawString(String.format(Locale.ROOT, "%.2f", graphLimits.getMaxY()),
								   getWidth() / 2 + 2, 0 + 10);
		g.drawString(String.format(Locale.ROOT, "%.2f", graphLimits.getMinY()),
								   getWidth() / 2 + 2, getHeight() - 5);
	}
	/**
	 * Draws a new graph according to the mathematical expression given as a
	 * parameter.
	 *
	 * @param expression	Mathematical expression for the graph as a string.
	 */
	public void drawGraph(String expression) {
		setGraphUpdate(true);
		setExpression(expression);
		setPostfixCalculator();
		setDefaultLimits();
		setGraph();
		resetCursor();
		repaint();
	}
	/**
	 * Removes graph from coordinates and resets limits to defaults.
	 */
	public void clearGraph() {
		initGraphPanel();
		repaint();
	}
	/**
	 * Updates coordinate limits according to parameters. For both x and y
	 * limits, the maximum limit must be higher than the minimum limit.
	 *
	 * @param minLimitForX	Minimum limit for x
	 * @param maxLimitForX	Maximum limit for x
	 * @param minLimitForY	Minimum limit for y
	 * @param maxLimitForY	Maximum limit for y
	 * @throws IllegalArgumentException	If minimum limit for either x or y is
	 * 									higher than the maximum limit
	 */
	public void updateCoordinates(double minLimitForX, double maxLimitForX,
								  double minLimitForY, double maxLimitForY)
										  	throws IllegalArgumentException {
		if ((minLimitForX > maxLimitForX) || (minLimitForY > maxLimitForY))
			throw new IllegalArgumentException(
									"Minimum limit for either x or y cannot "
								  + "be higher than the maximum limit!");
		updateLimits(minLimitForX, maxLimitForX, minLimitForY, maxLimitForY);
		setGraphUpdate(true);
		resetCursor();
		repaint();
	}
	/**
	 * Place cursor in view.
	 */
	public void enableCursor() {
		setShowCursor(true);
	}
	/**
	 * Remove cursor from view.
	 */
	public void disableCursor() {
		setShowCursor(false);
	}
	/**
	 * Moves cursor to left by one point along the graph. If new position for
	 * the cursor is not valid, i.e., the position does not exist, then finds
	 * the next valid position on graph. If there is no valid position on the
	 * left then cursor will remain on the old position.
	 */
	public void moveCursorToLeft() {
		if ((!cursorShow) || (cursorIndex == -1))
			return;
		for (int index = cursorIndex - 1; index >= 0; index--) {
			if (isValidCursorIndex(index)) {
				cursorIndex = index;
				cursorPoint = graph.getGraph().get(cursorIndex);
				break;
			}
		}
	}
	/**
	 * Moves cursor to right by one point along the graph. If new position for
	 * the cursor is not valid, i.e., the position does not exist, then finds
	 * the next valid position on graph. If there is no valid position on the
	 * right then cursor will remain on the old position.
	 */
	public void moveCursorToRight() {
		if ((!cursorShow) || (cursorIndex == -1))
			return;
		for (int index = cursorIndex + 1;
			 index < graph.getGraph().size();
			 index++) {
			if (isValidCursorIndex(index)) {
				cursorIndex = index;
				cursorPoint = graph.getGraph().get(cursorIndex);
				break;
			}
		}
	}
	/**
	 * Zoom out by increasing limits on x and y axes by 25%.
	 */
	public void zoomOut() {
		updateLimits(graphLimits.getMinX() * 1.25,
					 graphLimits.getMaxX() * 1.25,
					 graphLimits.getMinY() * 1.25,
					 graphLimits.getMaxY() * 1.25);
		setGraphUpdate(true);
		setCursorUpdate(true);
		repaint();
	}
	/**
	 * Zoom in by decreasing limits on x and y axes by 25%.
	 */
	public void zoomIn() {
		updateLimits(graphLimits.getMinX() * 0.75,
					 graphLimits.getMaxX() * 0.75,
					 graphLimits.getMinY() * 0.75,
					 graphLimits.getMaxY() * 0.75);
		setGraphUpdate(true);
		setCursorUpdate(true);
		repaint();
	}
	/**
	 * Center coordinates on cursor location.
	 */
	public void centerOnCursor() {
		if ((!cursorShow) || (cursorPoint == null))
			return;
		double xXiff = (graphLimits.getMaxX() - graphLimits.getMinX()) / 2;
		double yXiff = (graphLimits.getMaxY() - graphLimits.getMinY()) / 2;
		updateLimits(cursorPoint.x - xXiff, cursorPoint.x + xXiff,
					 cursorPoint.y - yXiff, cursorPoint.y + yXiff);
		setGraphUpdate(true);
		setCursorUpdate(true);
		repaint();
	}
	/**
	 * Restores coordinate limits back to default values for both x and y axes.
	 */
	public void resetLimits() {
		setDefaultLimits();
		setGraphUpdate(true);
		resetCursor();
		repaint();
	}
	/**
	 * Return minimum limit for x axis.
	 *
	 * @return	Minimum limit for x.
	 */
	public double getMinLimitForX() {
		return graphLimits.getMinX();
	}
	/**
	 * Return minimum limit for y axis.
	 *
	 * @return	Minimum limit for y.
	 */
	public double getMinLimitForY() {
		return graphLimits.getMinY();
	}
	/**
	 * Return maximum limit for x axis.
	 *
	 * @return	Maximum limit for x.
	 */
	public double getMaxLimitForX() {
		return graphLimits.getMaxX();
	}
	/**
	 * Return maximum limit for y axis.
	 *
	 * @return	Maximum limit for y.
	 */
	public double getMaxLimitForY() {
		return graphLimits.getMaxY();
	}
	/**
	 * Set indication that the panel was resized prompting an update to the
	 * graph.
	 */
	public void panelResized() {
		setGraphUpdate(true);
		setCursorUpdate(true);
	}
	/**
	 * Format and return user instructions string.
	 *
	 * @return Instructions for the user in String presentation.
	 */
	public String getUserInstructions() {
		return postfixSolver.getUserInstructions();
	}
}

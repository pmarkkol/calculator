/* BinaryComponent.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator.expression;

/**
 * This class represents a binary operator component of a mathematical
 * expression. Binary operators are operators that require two operands, e.g.,
 * addition, subtraction, multiplication, division and exponent.
 *
 * @author Paavo Markkola
 */
public class BinaryComponent extends Component {
	/**
	 * Instruction string for the user describing how to use the feature.
	 */
	private static String instructions =
		  "<span class=\"ins-strong\">Binary operators</span>. These "
		+ "operators combine two operands that surround them. Any number of "
		+ "spaces can be included with binary operators. Valid binary "
		+ "operators are <span class=\"ins-code\">+</span>, "
		+ "<span class=\"ins-code\">-</span>, <span class=\"ins-code\">*</span>"
		+ ", <span class=\"ins-code\">/</span> and <span class=\"ins-code\">^"
		+ "</span>.";
	/**
	 * Operator instance that actually perform the functionality of the binary
	 * operator.
	 */
	private Operator operator;
	/**
	 * Regular expression for binary components.
	 */
	public static String binaryOperatorRegEx = "(^\\s*[-+*/^]\\s*)";
	/**
	 * Construct a new BinaryComponent. Set correct Operator instance for the
	 * binary operator.
	 *
	 * @param component	String presentation of the component.
	 * @param position	Position of the component in a mathematical expression.
	 */
	public BinaryComponent(String component, int position) {
		super(component, position);
		setOperator();
	}
	/**
	 * Format and return user instructions string.
	 *
	 * @return Instructions for the user in String presentation.
	 */
	public static String getUserInstructions() {
		return instructions;
	}
	/**
	 * Set the corresponding Operator instance.
	 */
	private void setOperator() {
		for (OperatorType type : OperatorType.values())
			if (cString.getComponentString().equals(type.getOperator()))
				operator = type.create();
	}
	/**
	 * Return regular expression for binary components.
	 *
	 * @return	Regular expression.
	 */
	public static String getRegEx() {
		return binaryOperatorRegEx;
	}
	/**
	 * This component is a binary operator so return true.
	 *
	 * @return	True.
	 */
	@Override
	public boolean isBinaryOperator() {
		return true;
	}
	/**
	 * Check if the binary operator may a be unary sign. This concerns operators
	 * '-' and '+' which can be used as signs.
	 *
	 * @return	True if this binary component is '-' or '+', false otherwise.
	 */
	@Override
	public boolean isUnarySign() {
		if (cString.getComponentString().equals("-") ||
			cString.getComponentString().equals("+"))
			return true;
		else
			return false;
	}
	/**
	 * Binary operator cannot be at first position so return false.
	 *
	 * @return	False.
	 */
	@Override
	public boolean canBeFirst() {
		return false;
	}
	/**
	 * Binary operator cannot be at last position so return false.
	 *
	 * @return	False.
	 */
	@Override
	public boolean canBeLast() {
		return false;
	}
	/**
	 * Return precedence value of the binary operator.
	 *
	 * @return	Precedence value of the binary operator.
	 */
	@Override
	public int precedenceValue() {
		return operator.precedenceValue();
	}
	/**
	 * Execute the binary operation with the operands given as parameters.
	 *
	 * @param second	Second operand for the operator.
	 * @param first		First operand for the operator.
	 * @return	Result of the operation.
	 */
	@Override
	public double execute(double second, double first) {
		return operator.execute(second, first);
	}
}

/* EvaluationErrorException.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator.expression;

@SuppressWarnings("serial")
/**
 * This class is an exception that is thrown when mathematical expression
 * cannot be evaluated as such for some reason.
 *
 * @author Paavo Markkola
 */
public class EvaluationErrorException extends RuntimeException {
	/**
	 * Construct a new EvaluationErrorException with specific message.
	 *
	 * @param message	Message describing the cause for the exception.
	 */
	public EvaluationErrorException(String message) {
		super(message);
	}
}

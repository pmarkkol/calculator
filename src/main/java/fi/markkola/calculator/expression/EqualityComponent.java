/**
 *
 */
package fi.markkola.calculator.expression;

/**
 * This class represents an equality sign component of a mathematical
 * expression.
 *
 * @author Paavo Markkola
 */
public class EqualityComponent extends Component {
	/**
	 * Instruction string for the user describing how to use the feature.
	 */
	private static String instructions =
		  "<span class=\"ins-strong\">Equality string</span>, i.e., "
		+ "<span class=\"ins-code\">y=</span>. Including it is optional and it "
		+ "does not affect evaluations but can be included for clarity. If "
		+ "included it must be at the beginning of the expression and may "
		+ "contain any number of spaces.";
	/**
	 * Regular expression for number components.
	 */
	private static String equalityRegEx = "(^\\s*[yY]\\s*[=]\\s*)";
	/**
	 * Construct a new EqualityComponent.
	 *
	 * @param component	String presentation of the component.
	 * @param position	Position of the component in a mathematical expression.
	 */
	public EqualityComponent(String component, int position) {
		super(component, position);
	}
	/**
	 * Format and return user instructions string.
	 *
	 * @return Instructions for the user in String presentation.
	 */
	public static String getUserInstructions() {
		return instructions;
	}
	/**
	 * Return regular expression for number components.
	 *
	 * @return	Regular expression.
	 */
	public static String getRegEx() {
		return equalityRegEx;
	}
	/**
	 * Check if the component is an equality sign.
	 *
	 * @return	True if the component is an equality sign, false otherwise.
	 */
	@Override
	public boolean isEquality() {
		return true;
	}
	/**
	 * Check if the component can be at first position in an expression.
	 *
	 * @return	True if the component can be at position, false otherwise.
	 */
	@Override
	public boolean canBeFirst() {
		return true;
	}
	/**
	 * Check if the component can be at last position in an expression.
	 *
	 * @return	True if the component can be at position, false otherwise.
	 */
	@Override
	public boolean canBeLast() {
		return false;
	}
}

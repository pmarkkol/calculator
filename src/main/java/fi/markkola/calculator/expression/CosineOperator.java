/* CosineOperator.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator.expression;

/**
 * This class represents cosine operator.
 *
 * @author Paavo Markkola
 */
public class CosineOperator extends Operator {
	/**
	 * String presentation of the cosine operator.
	 */
	private static String operator = "cos";
	/**
	 * Static method that returns string presentation of the cosine operator.
	 *
	 * @return	String presentation of the cosine operator.
	 */
	public static String getOperator() {
		return operator;
	}
	/**
	 * Return precedence value of the cosine operator.
	 *
	 * @return	Precedence value of the cosine operator.
	 */
	@Override
	public int precedenceValue() {
		return 4;
	}
	/**
	 * Execute the cosine operator for one operand that is given as parameter,
	 * and return the results.
	 *
	 * @param operand	Operand for the operator.
	 * @return	Result of the operation.
	 */
	@Override
	public double execute(double operand) {
		return Math.cos(operand);
	}
}

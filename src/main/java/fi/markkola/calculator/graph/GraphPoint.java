/* GraphPoint.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator.graph;

/**
 * This class represents a discrete point of a graph on a Cartesian coordinate
 * system, with x and y axes, that is mapped to a graphical presentation on a
 * canvas.
 *
 * @author Paavo Markkola
 */
public class GraphPoint {
	/**
	 * Horizontal coordinate of the point
	 */
	public double x;
	/**
	 * Vertical coordinate of the point
	 */
	public double y;
	/**
	 * Horizontal position of the point on the canvas
	 */
	public int xPixel;
	/**
	 * Vertical position of the point on the canvas
	 */
	public int yPixel;
	/**
	 * Construct a new GraphPoint.
	 *
	 * @param xValue		Horizontal coordinate of the point
	 * @param yValue		Vertical coordinate of the point
	 * @param xPixelValue	Horizontal position of the point on the canvas
	 * @param yPixelValue	Vertical position of the point on the canvas
	 */
	public GraphPoint(double xValue, double yValue,
					  int xPixelValue, int yPixelValue) {
		x = xValue;
		y = yValue;
		xPixel = xPixelValue;
		yPixel = yPixelValue;
	}
}

/* NumberComponent.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator.expression;

/**
 * This class represents a number component of a mathematical expression.
 *
 * @author Paavo Markkola
 */
public class NumberComponent extends Component {
	/**
	 * Instruction string for the user describing how to use the feature.
	 */
	private static String instructions =
		    "<span class=\"ins-strong\">Numbers</span>. Valid numbers are in "
		  + "decimal format. Decimal separator is dot "
		  + "(<span class=\"ins-code\">.<span>).";
	/**
	 * Numerical value of the component.
	 */
	private double value;
	/**
	 * Regular expression for number components.
	 */
	public static String numberRegEx = "(^\\s*\\d*\\.?\\d+\\s*)|"
								+ "(^\\s*\\(\\s*-\\s*\\d*\\.?\\d+\\s*\\)\\s*)";
	/**
	 * Construct a new NumberComponent. Parse the string presentation to a
	 * double and store the result.
	 *
	 * @param component	String presentation of the component.
	 * @param position	Position of the component in a mathematical expression.
	 */
	public NumberComponent(String component, int position) {
		super(component, position);
		parseNumberValue();
	}
	/**
	 * Format and return user instructions string.
	 *
	 * @return Instructions for the user in String presentation.
	 */
	public static String getUserInstructions() {
		return instructions;
	}
	/**
	 * Parse the string presentation of the component to a double and store the
	 * result. If the string is not a double use Double.NaN (not a Number)
	 * instead.
	 */
	private void parseNumberValue() {
		try {
			setNumberValue(Double.parseDouble(cString.getComponentString()));
		} catch (NumberFormatException e) {
			setNumberValue(Double.NaN);
		}
	}
	/**
	 * Set numerical value for the component.
	 *
	 * @param value	Numerical value for the component.
	 */
	private void setNumberValue(double value) {
		this.value = value;
	}
	/**
	 * Return regular expression for number components.
	 *
	 * @return	Regular expression.
	 */
	public static String getRegEx() {
		return numberRegEx;
	}
	/**
	 * Returns string presentation of the number.
	 *
	 * @return	String presentation of the number.
	 */
	@Override
	public String toString() {
		return Double.toString(value);
	}
	/**
	 * This component is a number so return true.
	 *
	 * @return	True.
	 */
	@Override
	public boolean isNumber() {
		return true;
	}
	/**
	 * Return numerical value of the component.
	 *
	 * @return	Numerical value of the component.
	 */
	@Override
	public double getValue() {
		return value;
	}
}

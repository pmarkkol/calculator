/* LeftParenthesisComponent.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator.expression;

/**
 * This class represents a left parenthesis, i.e., '(', of a mathematical
 * expression. Parentheses can be used to signify a different precedence of
 * operators.
 *
 * @author Paavo Markkola
 */
public class LeftParenthesisComponent extends Component {
	/**
	 * Instruction string for the user describing how to use the feature.
	 */
	private static String instructions =
		  "<span class=\"ins-strong\">Left parenthesis</span>, i.e., "
		+ "<span class=\"ins-code\">(</span>. Opens a parenthesis sequence "
		+ "that can be used to change the precedence of operators in an "
		+ "expression.";
	/**
	 * Regular expression for left parenthesis components.
	 */
	public static String leftParenthesisRegEx = "(^\\s*[(]\\s*)";
	/**
	 * Construct a new LeftParenthesisComponent.
	 *
	 * @param component	String presentation of the component.
	 * @param position	Position of the component in a mathematical expression.
	 */
	public LeftParenthesisComponent(String component, int position) {
		super(component, position);
	}
	/**
	 * Format and return user instructions string.
	 *
	 * @return Instructions for the user in String presentation.
	 */
	public static String getUserInstructions() {
		return instructions;
	}
	/**
	 * Return regular expression for left parenthesis components.
	 *
	 * @return	Regular expression.
	 */
	public static String getRegEx() {
		return leftParenthesisRegEx;
	}
	/**
	 * Returns string presentation of the left parenthesis.
	 *
	 * @return	String presentation of the left parenthesis.
	 */
	@Override
	public String toString() {
		return "(";
	}
	/**
	 * This component is a left parenthesis so return true.
	 *
	 * @return	True.
	 */
	@Override
	public boolean isLeftParenthesis() {
		return true;
	}
	/**
	 * Left parenthesis component can be at last position in an expression.
	 *
	 * @return	False.
	 */
	@Override
	public boolean canBeLast() {
		return false;
	}

}

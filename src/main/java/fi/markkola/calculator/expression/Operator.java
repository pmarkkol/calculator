/* Operator.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator.expression;

/**
 * This abstract class represent an operator that may be binary or unary
 * operator.
 *
 * @author Paavo Markkola
 */
public abstract class Operator {
	/**
	 * Static method that returns string presentation of the operator.
	 *
	 * @return	String presentation of the operator.
	 */
	public static String getOperator() {
		return null;
	}
	/**
	 * Return precedence value of the operator.
	 *
	 * @return	Precedence value of the operator.
	 */
	public int precedenceValue() {
		return -1;
	}
	/**
	 * Execute the operator for one operand that is given as parameter, and
	 * return the results. This method is valid only for unary operators that
	 * require exactly one operand.
	 *
	 * @param operand	Operand for the operator.
	 * @return	Result of the operation.
	 */
	public double execute(double operand) {
		return Double.NaN;
	}
	/**
	 * Execute the operator for two operand that are given as parameter, and
	 * return the results. This method is valid only for binary operators that
	 * require exactly two operands.
	 *
	 * @param secondOperand	Second operand for the operator.
	 * @param firstOperand	First operand for the operator.
	 * @return	Result of the operation.
	 */
	public double execute(double secondOperand, double firstOperand) {
		return Double.NaN;
	}
}

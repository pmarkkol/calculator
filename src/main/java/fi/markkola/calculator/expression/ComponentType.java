/* ComponentType.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator.expression;

/**
 * Enum class for different types of components in mathematical expressions.
 * Also, serves as factory class for the components.
 *
 * @author Paavo Markkola
 */
public enum ComponentType {
	EQUALITY,
	NUMBER,
	CONSTANT,
	VARIABLE,
	BINARY_OPERATOR,
	UNARY_OPERATOR,
	LEFT_PARENTHESIS,
	RIGHT_PARENTHESIS;
	/**
	 * Returns regular expression that can be used identify the component from
	 * an expression string.
	 *
	 * @return	RegEx string for the component.
	 */
	public String getRegEx() {
		switch (this) {
		case EQUALITY:
			return EqualityComponent.getRegEx();
		case NUMBER:
			return NumberComponent.getRegEx();
		case CONSTANT:
			return ConstantComponent.getRegEx();
		case VARIABLE:
			return VariableComponent.getRegEx();
		case BINARY_OPERATOR:
			return BinaryComponent.getRegEx();
		case UNARY_OPERATOR:
			return UnaryComponent.getRegEx();
		case LEFT_PARENTHESIS:
			return LeftParenthesisComponent.getRegEx();
		case RIGHT_PARENTHESIS:
			return RightParenthesisComponent.getRegEx();
		default:
			return null;
		}
	}
	/**
	 * Factory method for expression components. Constructs a new instance of
	 * specific component type.
	 *
	 * @param component	String presentation of the component as part of a
	 * 					mathematical expression.
	 * @param position	Position of the component in an expression string.
	 * @return			New instance of a component subclass.
	 */
	public Component create(String component, int position) {
		switch (this) {
		case EQUALITY:
			return new EqualityComponent(component, position);
		case NUMBER:
			return new NumberComponent(component, position);
		case CONSTANT:
			return new ConstantComponent(component, position);
		case VARIABLE:
			return new VariableComponent(component, position);
		case BINARY_OPERATOR:
			return new BinaryComponent(component, position);
		case UNARY_OPERATOR:
			return new UnaryComponent(component, position);
		case LEFT_PARENTHESIS:
			return new LeftParenthesisComponent(component, position);
		case RIGHT_PARENTHESIS:
			return new RightParenthesisComponent(component, position);
		default:
			return null;
		}
	}
	/**
	 * Returns user instruction string from each component type.
	 *
	 * @return Instructions for the user in String presentation.
	 */
	public String getUserInstructions() {
		switch (this) {
		case EQUALITY:
			return EqualityComponent.getUserInstructions();
		case NUMBER:
			return NumberComponent.getUserInstructions();
		case CONSTANT:
			return ConstantComponent.getUserInstructions();
		case VARIABLE:
			return VariableComponent.getUserInstructions();
		case BINARY_OPERATOR:
			return BinaryComponent.getUserInstructions();
		case UNARY_OPERATOR:
			return UnaryComponent.getUserInstructions();
		case LEFT_PARENTHESIS:
			return LeftParenthesisComponent.getUserInstructions();
		case RIGHT_PARENTHESIS:
			return RightParenthesisComponent.getUserInstructions();
		default:
			return null;
		}
	}
}

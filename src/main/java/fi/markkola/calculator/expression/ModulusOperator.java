/* ModulusOperator.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator.expression;

/**
 * This class represents modulus, the absolute value of a real number (|a|),
 * operator.
 *
 * @author Paavo Markkola
 */
public class ModulusOperator extends Operator {
	/**
	 * String presentation of the modulus operator.
	 */
	private static String operator = "abs";
	/**
	 * Static method that returns string presentation of the modulus
	 * operator.
	 *
	 * @return	String presentation of the modulus operator.
	 */
	public static String getOperator() {
		return operator;
	}
	/**
	 * Return precedence value of the modulus operator.
	 *
	 * @return	Precedence value of the modulus operator.
	 */
	@Override
	public int precedenceValue() {
		return 4;
	}
	/**
	 * Execute the modulus operator for one operand that is given as
	 * parameter, and return the results.
	 *
	 * @param operand	Operand for the operator.
	 * @return	Result of the operation.
	 */
	@Override
	public double execute(double operand) {
		return Math.abs(operand);
	}
}

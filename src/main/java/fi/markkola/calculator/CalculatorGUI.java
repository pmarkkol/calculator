/* CalculatorGUI.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.Locale;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.plaf.basic.BasicArrowButton;

import fi.markkola.calculator.expression.InvalidExpressionException;

/**
 * This class is the graphical user interface (GUI) for the Calculator
 * application.
 *
 * @author Paavo Markkola
 */
public class CalculatorGUI {
	/**
	 * JFrame that contains all other components.
	 */
	private JFrame frmCalculatorGUI;
	/**
	 * JPanel components and variants that contain all functionality within
	 * the application.
	 */
	private JPanel expressionPanel;
	private GraphPanel graphPanel;
	private JPanel controlPanel;
	private JPanel cursorPanel;
	private JPanel limitsPanel;
	/**
	 * Layout for the Jframe is GridBagLayout.
	 */
	private GridBagLayout gridBagLayout;
	private GridBagConstraints gbc_expressionPanel;
	private GridBagConstraints gbc_drawPanel;
	private GridBagConstraints gbc_controlPanel;
	/**
	 * Text field components for user input are needed for the graph's
	 * expression as well as for coordinate limits.
	 */
	private JTextField textFieldExpression;
	private JTextField textFieldMinX;
	private JTextField textFieldMaxX;
	private JTextField textFieldMinY;
	private JTextField textFieldMaxY;
	/**
	 * Action listeners for buttons and other components.
	 */
	private ActionListener expressionListener;
	private ActionListener cursorListener;
	private ActionListener coordinateListener;
	private ComponentAdapter componentAdapter;
	/**
	 * Each component with listener attach has a specific command string to
	 * to identify specific action.
	 */
	private final String expressionFieldCommand = "Expression field";
	private final String drawCommand = "Draw";
	private final String clearCommand = "Clear";
	private final String helpCommand = "Help";
	private final String updateCommand = "Update";
	private final String showCursorCommand = "Show cursor";
	private final String cursorLeftCommand = "Cursor left";
	private final String cursorRightCommand = "Cursor right";
	private final String zoomOutCommand = "Zoom out";
	private final String zoomInCommand = "Zoom in";
	private final String resetCommand = "Reset";
	private final String centerCommand = "Center";
	/**
	 * HTML template for InvalidExpressionException message to be used with
	 * the related error dialog.
	 */
	private final String exceptionTemplate =  "<html>"
											+ "<header>"
											+ "<style>"
											+ ".excp-error {"
											+ "    font-weight:bold;"
											+ "    color:red;"
											+ "    font-family:monospace;"
											+ "}"
											+ "</style>"
											+ "</header>"
											+ "<body>"
											+ "<div class=\"excp-main\">"
											+ "<div class=\"excp-msg\">"
											+ "<p>%s:</p><br>"
											+ "</div>"
											+ "<div class=\"excp-error\">"
											+ "<p>%s<br>%s^</p>"
											+ "</div>"
											+ "</div>"
											+ "</body>"
											+ "</html>";
	/**
	 * HTML message format for showing user instructions.
	 */
	private final String userInstructions =   "<html>"
											+ "<header>"
											+ "<style>"
											+ ".ins-main {"
											+ "    width:500px;"
											+ "}"
											+ ".ins-strong {"
											+ "    font-weight:bold;"
											+ "}"
											+ ".ins-code {"
											+ "    font-weight:bold;"
											+ "    color:red;"
											+ "    font-family:monospace;"
											+ "}"
											+ ".ins-list {"
											+ "    margin-left: 20px;"
											+ "}"
											+ "</style>"
											+ "</header>"
											+ "<body>"
											+ "%s"
											+ "</body>"
											+ "</html>";
	/**
	 * Create new CalculatorGUI instance. Set look and feel to system specific.
	 * Set event dispatch thread to initialize the GUI.
	 */
	public CalculatorGUI() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}

		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					initializeCalculatorGUI();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	/**
	 * Initialize the contents of the GUI.
	 */
	private void initializeCalculatorGUI() {
		initActionListener();
		initGridBagLayout();
		initExpressionPanel();
		initGraphPanel();
		initControlPanel();
		initCalculatorFrame();
	}
	/**
	 * Initialize JFrame frmCalculatorGUI for the GUI. Layout used is
	 * GridBagLayout and it consist of three JPanels - expression panel,
	 * graph panel and control panel.
	 * <p>
	 * The expression panel contains the JTextfield element to enter the
	 * expression for a graph to be drawn. The graph panel is used to draw the
	 * actual graph with coordinates etc. and the control panel contains
	 * various elements for controlling the output.
	 */
	private void initCalculatorFrame() {
		frmCalculatorGUI = new JFrame();
		frmCalculatorGUI.setTitle("Calculator\r\n");
		frmCalculatorGUI.setBounds(200, 200, 526, 698);
		frmCalculatorGUI.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frmCalculatorGUI.getContentPane().setLayout(gridBagLayout);
		frmCalculatorGUI.getContentPane().add(expressionPanel,
											  gbc_expressionPanel);
		frmCalculatorGUI.getContentPane().add(graphPanel, gbc_drawPanel);
		frmCalculatorGUI.getContentPane().add(controlPanel, gbc_controlPanel);

		frmCalculatorGUI.setLocationRelativeTo(null);
		frmCalculatorGUI.setVisible(true);
	}
	/**
	 * Initialize the expression panel. Layout for the panel is FlowLayout.
	 * <p>
	 * The panel consist of JtextField for entering the expression for a graph
	 * as well as JButtons for initiate drawing of the graph and resetting
	 * the graph.
	 */
	private void initExpressionPanel() {
		expressionPanel = new JPanel();

		JLabel lblExpression = new JLabel("Expression: y=");
		expressionPanel.add(lblExpression);

		textFieldExpression = new JTextField();
		textFieldExpression.setColumns(30);
		textFieldExpression.setActionCommand(expressionFieldCommand);
		textFieldExpression.addActionListener(expressionListener);
		expressionPanel.add(textFieldExpression);

		JButton btnHelp = new JButton("?");
		btnHelp.setActionCommand(helpCommand);
		btnHelp.addActionListener(expressionListener);
		btnHelp.setMargin(new java.awt.Insets(1, 10, 1, 10));
		expressionPanel.add(btnHelp);

		JButton btnDraw = new JButton("Draw");
		btnDraw.setActionCommand(drawCommand);
		btnDraw.addActionListener(expressionListener);
		expressionPanel.add(btnDraw);

		JButton btnClear = new JButton("Clear");
		btnClear.setActionCommand(clearCommand);
		btnClear.addActionListener(expressionListener);
		expressionPanel.add(btnClear);
	}
	/**
	 * Initialize GraphPanel graphPanel.
	 * <p>
	 * The panel is responsible for all graph related output.
	 */
	private void initGraphPanel() {
		graphPanel = new GraphPanel();
		graphPanel.addComponentListener(componentAdapter);
	}
	/**
	 * Initialize the control panel. Layout for the panel is FlowLayout.
	 * <p>
	 * The panel consist of two separate panels, JPanel cursorPanel and JPanel
	 * limitsPanel. The purpose for this panel is act as container for the two
	 * panels mentioned.
	 */
	private void initControlPanel() {
		controlPanel = new JPanel();
		controlPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		cursorPanel = new JPanel();
		controlPanel.add(cursorPanel);
		initCursorPanel();

		limitsPanel = new JPanel();
		controlPanel.add(limitsPanel);
		initLimitsPanel();
	}
	/**
	 * Initialize JPanel cursorPanel. Layout for the panel is FlowLayout.
	 * <p>
	 * The panel consist of controls for cursor feature of the application.
	 */
	private void initCursorPanel() {
		JCheckBox chckbxShowCursor = new JCheckBox("Show cursor");
		chckbxShowCursor.setHorizontalAlignment(SwingConstants.LEFT);
		chckbxShowCursor.setHorizontalTextPosition(SwingConstants.LEFT);
		chckbxShowCursor.setActionCommand(showCursorCommand);
		chckbxShowCursor.addActionListener(cursorListener);
		cursorPanel.add(chckbxShowCursor);

		JLabel lblMoveCursor = new JLabel("Move cursor:");
		cursorPanel.add(lblMoveCursor);

		BasicArrowButton cursorLeft =
									new BasicArrowButton(BasicArrowButton.WEST);
		cursorLeft.setActionCommand(cursorLeftCommand);
		cursorLeft.addActionListener(cursorListener);
		cursorPanel.add(cursorLeft);

		BasicArrowButton cursorRight =
									new BasicArrowButton(BasicArrowButton.EAST);
		cursorRight.setActionCommand(cursorRightCommand);
		cursorRight.addActionListener(cursorListener);
		cursorPanel.add(cursorRight);

		JButton centerOnCursor = new JButton("Center");
		centerOnCursor.setActionCommand(centerCommand);
		centerOnCursor.addActionListener(coordinateListener);
		cursorPanel.add(centerOnCursor);

		JLabel lblZoom = new JLabel("Zoom:");
		cursorPanel.add(lblZoom);

		JButton zoomOut = new JButton("-");
		zoomOut.setActionCommand(zoomOutCommand);
		zoomOut.addActionListener(coordinateListener);
		cursorPanel.add(zoomOut);

		JButton zoomIn = new JButton("+");
		zoomIn.setActionCommand(zoomInCommand);
		zoomIn.addActionListener(coordinateListener);
		cursorPanel.add(zoomIn);

		JButton btnReset = new JButton("Reset");
		btnReset.setActionCommand(resetCommand);
		btnReset.addActionListener(coordinateListener);
		cursorPanel.add(btnReset);
	}
	/**
	 * Initialize the limits panel. Layout for the panel is FlowLayout.
	 * <p>
	 * The panel consist of controls for setting the limits of the coordinate
	 * system.
	 */
	private void initLimitsPanel() {
		JLabel lblMaxX = new JLabel("Min X:");
		limitsPanel.add(lblMaxX);

		textFieldMinX = new JTextField();
		textFieldMinX.setColumns(7);
		limitsPanel.add(textFieldMinX);

		JLabel lblMaxY = new JLabel("Max X:");
		limitsPanel.add(lblMaxY);

		textFieldMaxX = new JTextField();
		textFieldMaxX.setColumns(7);
		limitsPanel.add(textFieldMaxX);

		JLabel lblMinY = new JLabel("Min Y:");
		limitsPanel.add(lblMinY);

		textFieldMinY = new JTextField();
		textFieldMinY.setColumns(7);
		limitsPanel.add(textFieldMinY);

		JLabel lblMaxY_1 = new JLabel("Max Y:");
		limitsPanel.add(lblMaxY_1);

		textFieldMaxY = new JTextField();
		textFieldMaxY.setColumns(7);
		limitsPanel.add(textFieldMaxY);

		JButton btnUpdate = new JButton("Update");
		btnUpdate.setActionCommand(updateCommand);
		btnUpdate.addActionListener(coordinateListener);
		limitsPanel.add(btnUpdate);

		setLimitValues();
	}
	/**
	 * Initializes ActionListener for CalculatorGUI that handles all actions
	 * for the application.
	 */
	private void initActionListener() {

		expressionListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				String actionString = actionEvent.getActionCommand();
				if ((actionString.equals(drawCommand)) ||
					(actionString.equals(expressionFieldCommand)))
					updateExpression();
				else if (actionString.equals(clearCommand))
					graphPanel.clearGraph();
				else if (actionString.equals(helpCommand)) {
					showUserInstructions();
				}
				setLimitValues();
			}

			private void showUserInstructions() {
				JOptionPane.showMessageDialog(
						frmCalculatorGUI,
						String.format(userInstructions,
									  graphPanel.getUserInstructions()),
						"Expression help", JOptionPane.INFORMATION_MESSAGE);
			}

			private void updateExpression() {
				try {
					graphPanel.drawGraph(textFieldExpression.getText());
				} catch  (InvalidExpressionException exception) {
					JOptionPane.showMessageDialog(frmCalculatorGUI,
									formatInvalidExpressionMessage(exception),
									"Invalid equation",
									JOptionPane.ERROR_MESSAGE);
				}
			}

			private String formatInvalidExpressionMessage(
										InvalidExpressionException exception) {
				if (exception.getExpression().isEmpty())
					return exception.getMessage();
				String spaces = "";
				for (int i = 0; i < exception.getPosition(); i++)
					spaces += "&nbsp;";
				return String.format(exceptionTemplate, exception.getMessage(),
									 exception.getExpression(), spaces);
			}
		};

		cursorListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				String actionString = actionEvent.getActionCommand();
				if (actionString.equals(showCursorCommand))
					if (((AbstractButton) actionEvent.getSource()).
														getModel().isSelected())
						graphPanel.enableCursor();
					else
						graphPanel.disableCursor();
				else if (actionString.equals(cursorLeftCommand))
					graphPanel.moveCursorToLeft();
				else if (actionString.equals(cursorRightCommand))
					graphPanel.moveCursorToRight();
			}
		};

		coordinateListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				String actionString = actionEvent.getActionCommand();
				if (actionString.equals(updateCommand))
					updateCoordinateLimits();
				else if (actionString.equals(zoomOutCommand))
					graphPanel.zoomOut();
				else if (actionString.equals(zoomInCommand))
					graphPanel.zoomIn();
				else if (actionString.equals(resetCommand))
					graphPanel.resetLimits();
				else if (actionString.equals(centerCommand))
					graphPanel.centerOnCursor();
				setLimitValues();
			}

			private void updateCoordinateLimits() {
				try {
					double newMinX, newMaxX, newMinY, newMaxY;
					newMinX = Double.valueOf(textFieldMinX.getText());
					newMaxX = Double.valueOf(textFieldMaxX.getText());
					newMinY = Double.valueOf(textFieldMinY.getText());
					newMaxY = Double.valueOf(textFieldMaxY.getText());
					graphPanel.updateCoordinates(newMinX, newMaxX,
												 newMinY, newMaxY);
				} catch  (NumberFormatException exception) {
					String errorMessage = String.format(
										"<html>" +
										"Invalid value for double: " +
										"<font color=\"red\">%s</font>" +
										"</html>",
						exception.getMessage().substring(exception.getMessage().
														 indexOf("\"")).
															replace("\"", ""));
					JOptionPane.showMessageDialog(frmCalculatorGUI,
												  errorMessage,
												  "Number format error",
												  JOptionPane.ERROR_MESSAGE);
				} catch  (IllegalArgumentException exception) {
					JOptionPane.showMessageDialog(frmCalculatorGUI,
												  exception.getMessage(),
												  "Invalid parameters",
												  JOptionPane.ERROR_MESSAGE);
				}
			}
		};

		componentAdapter = new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent componentEvent) {
				graphPanel.panelResized();
			}
		};
	}
	/**
	 * Initialize GridBagConstrain instances for all panels in application.
	 */
	private void initGridBagLayout() {
		gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{20};
		gridBagLayout.rowHeights = new int[]{20, 457, 0};
		gridBagLayout.columnWeights = new double[]{1.0};
		gridBagLayout.rowWeights = new double[]{0.0, 1.0, 1.0};

		gbc_expressionPanel = new GridBagConstraints();
		gbc_expressionPanel.insets = new Insets(10, 10, 5, 10);
		gbc_expressionPanel.fill = GridBagConstraints.BOTH;
		gbc_expressionPanel.gridx = 0;
		gbc_expressionPanel.gridy = 0;

		gbc_drawPanel = new GridBagConstraints();
		gbc_drawPanel.insets = new Insets(5, 10, 5, 10);
		gbc_drawPanel.fill = GridBagConstraints.BOTH;
		gbc_drawPanel.gridx = 0;
		gbc_drawPanel.gridy = 1;

		gbc_controlPanel = new GridBagConstraints();
		gbc_controlPanel.insets = new Insets(5, 10, 10, 10);
		gbc_controlPanel.fill = GridBagConstraints.BOTH;
		gbc_controlPanel.gridx = 0;
		gbc_controlPanel.gridy = 2;
	}
	/**
	 * Update limits text fields with current content.
	 */
	private void setLimitValues() {
		textFieldMinX.setText(String.format(Locale.ROOT, "%.2f",
											graphPanel.getMinLimitForX()));
		textFieldMinY.setText(String.format(Locale.ROOT, "%.2f",
											graphPanel.getMinLimitForY()));
		textFieldMaxX.setText(String.format(Locale.ROOT, "%.2f",
											graphPanel.getMaxLimitForX()));
		textFieldMaxY.setText(String.format(Locale.ROOT, "%.2f",
											graphPanel.getMaxLimitForY()));
	}
}




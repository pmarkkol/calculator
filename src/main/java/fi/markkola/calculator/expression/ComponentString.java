/* ComponentString.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator.expression;

/**
 * Class that is used to hold the string presentation of a component within a
 * mathematical expression. Also keeps count of the position of the component
 * within the expression as well as the length of the component string.
 * <p>
 * All fields can changed later after an instance has been constructed.
 *
 * @author Paavo Markkola
 */
public class ComponentString {
	private String componentString;
	private int componentLength;
	private int componentPosition;
	/**
	 * Regular expression for all white spaces.
	 */
	private String whiteSpaceRegEx = "\\s+";
	/**
	 * Regular expression for parentheses.
	 */
	private String parenthesesRegEx = "[()]";
	/**
	 * Construct a ComponentString from given string and position.
	 *
	 * @param component	String presentation of the expression component.
	 * @param position	Position of the component within an expression.
	 */
	public ComponentString(String component, int position) {
		setComponentString(component);
		setComponentLength(component.length());
		setComponentPosition(position);
	}
	/**
	 * Set new component string.
	 *
	 * @param component	String presentation of the expression component.
	 */
	public void setComponentString(String component) {
		componentString = component;
	}
	/**
	 * Set new length for component string.
	 *
	 * @param length	Length of the component string.
	 */
	public void setComponentLength(int length) {
		componentLength = length;
	}
	/**
	 * Set new position for the component string within a mathematical
	 * expression.
	 *
	 * @param position	Position of the component within an expression.
	 */
	public void setComponentPosition(int position) {
		componentPosition = position;
	}
	/**
	 * Return component string.
	 *
	 * @return	String presentation of the component.
	 */
	public String getComponentString() {
		return componentString;
	}
	/**
	 * Return length of the component string.
	 *
	 * @return	Length of the component string.
	 */
	public int getComponentLength() {
		return componentLength;
	}
	/**
	 * Return position of the component string within a mathematical expression.
	 * @return	Position of the component within an expression.
	 */
	public int getComponentPosition() {
		return componentPosition;
	}
	/**
	 * Use regEx to replace all whitespace in the component string with empty
	 * characters.
	 */
	public void removeWhiteSpace() {
		componentString = componentString.replaceAll(whiteSpaceRegEx, "");
	}
	/**
	 * Use regEx to replace all parentheses, i.e, '(' and ')', in the component
	 * string with empty characters.
	 */
	public void removeParentheses() {
		componentString = componentString.replaceAll(parenthesesRegEx, "");
	}

}
/* OperatorType.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator.expression;

/**
 * Enum class for different types of operators in mathematical expressions.
 * Also, serves as factory class for the operators.
 *
 * @author Paavo Markkola
 */
public enum OperatorType {

	ADDITION,			// +
	SUBSTRACTION,		// -
	MULTIPLICATION,		// *
	DIVISION,			// /
	EXPONENT,			// ^
	UNARY_PLUS,			// +
	UNARY_MINUS,		// -
	MODULUS,			// abs
	SQUARE_ROOT,		// sqr
	SINE,				// sin
	COSINE,				// cos
	TANGENT;			// tan
	/**
	 * Return string presentation of the operator.
	 *
	 * @return String presentation of the operator.
	 */
	public String getOperator() {
		switch (this) {
		case ADDITION:
			return AdditionOperator.getOperator();
		case SUBSTRACTION:
			return SubtractionOperator.getOperator();
		case MULTIPLICATION:
			return MultiplicationOperator.getOperator();
		case DIVISION:
			return DivisionOperator.getOperator();
		case EXPONENT:
			return ExponentOperator.getOperator();
		case UNARY_PLUS:
			return UnarySignPlusOperator.getOperator();
		case UNARY_MINUS:
			return UnarySignMinusOperator.getOperator();
		case MODULUS:
			return ModulusOperator.getOperator();
		case SQUARE_ROOT:
			return SquareRootOperator.getOperator();
		case SINE:
			return SineOperator.getOperator();
		case COSINE:
			return CosineOperator.getOperator();
		case TANGENT:
			return TangentOperator.getOperator();
		default:
			return null;
		}
	}
	/**
	 * Factory method for operators. Constructs a new instance of specific
	 * operator.
	 *
	 * @return	New instance of a operator subclass.
	 */
	public Operator create() {
		switch (this) {
		case ADDITION:
			return new AdditionOperator();
		case SUBSTRACTION:
			return new SubtractionOperator();
		case MULTIPLICATION:
			return new MultiplicationOperator();
		case DIVISION:
			return new DivisionOperator();
		case EXPONENT:
			return new ExponentOperator();
		case UNARY_PLUS:
			return new UnarySignPlusOperator();
		case UNARY_MINUS:
			return new UnarySignMinusOperator();
		case MODULUS:
			return new ModulusOperator();
		case SQUARE_ROOT:
			return new SquareRootOperator();
		case SINE:
			return new SineOperator();
		case COSINE:
			return new CosineOperator();
		case TANGENT:
			return new TangentOperator();
		default:
			return null;
		}
	}

}

/* Component.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator.expression;

/**
 * This abstract class represents a component of a mathematical expression.
 * <p>
 * A component in mathematical expression may be a number, variable, constant
 * or an operator. This class includes fields to contain string presentation of
 * component as well as methods to get information about the component.
 *
 * @author Paavo Markkola
 */
public abstract class Component {
	/**
	 * String presentation of the component.
	 */
	protected ComponentString cString;
	/**
	 * Format and return user instructions string.
	 *
	 * @return	Instructions for the user in String presentation.
	 */
	public static String getUserInstructions() {
		return null;
	}
	/**
	 * Construct new component. Parameters needed are the string presentation
	 * of the component and the position of the component in the original
	 * mathematical expression string.
	 *
	 * @param component	String presentation of the component.
	 * @param position	Position of the component in the expression.
	 */
	public Component(String component, int position) {
		cString = new ComponentString(component, position);
		cString.removeWhiteSpace();
		cString.removeParentheses();
	}
	/**
	 * Returns regular expression that can be used to parse the component from
	 * a string representing a mathematical expression.
	 *
	 * @return	Regular expression string.
	 */
	public static String getRegEx() {
		return null;
	}
	/**
	 * Returns string presentation of the component.
	 *
	 * @return	String presentation of the component.
	 */
	@Override
	public String toString() {
		return cString.getComponentString();
	}
	/**
	 * Returns position of the component in the original unparsed string
	 * representing a mathematical expression.
	 *
	 * @return	Position of the component.
	 */
	public int getPosition() {
		return cString.getComponentPosition();
	}
	/**
	 * Returns length of the unmodified string presentation of the component.
	 *
	 * @return	Length of the component as string.
	 */
	public int getLength() {
		return cString.getComponentLength();
	}
	/**
	 * Check if the component is an equality sign.
	 *
	 * @return	True if the component is an equality sign, false otherwise.
	 */
	public boolean isEquality() {
		return false;
	}
	/**
	 * Check if the component is a variable.
	 *
	 * @return	True if the component is a variable, false otherwise.
	 */
	public boolean isVariable() {
		return false;
	}
	/**
	 * Check if the component is a number.
	 *
	 * @return	True if the component is a number, false otherwise.
	 */
	public boolean isNumber() {
		return false;
	}
	/**
	 * Check if the component is a mathematical constant such as pi.
	 *
	 * @return	True if the component is a constant, false otherwise.
	 */
	public boolean isConstant() {
		return false;
	}
	/**
	 * Check if the component is an unary operator such as square root.
	 *
	 * @return	True if the component is an unary operator, false otherwise.
	 */
	public boolean isUnaryOperator() {
		return false;
	}
	/**
	 * Check if the component is a binary operator such as division
	 * operator.
	 *
	 * @return	True if the component is a binary operator, false otherwise.
	 */
	public boolean isBinaryOperator() {
		return false;
	}
	/**
	 * Check if the component is left parenthesis, i.e., "(".
	 *
	 * @return	True if the component is a left parenthesis, false otherwise.
	 */
	public boolean isLeftParenthesis() {
		return false;
	}
	/**
	 * Check if the component is right parenthesis, i.e., ")".
	 *
	 * @return	True if the component is a right parenthesis, false otherwise.
	 */
	public boolean isRightParenthesis() {
		return false;
	}
	/**
	 * Check if a binary operator may also be a unary operator, e.g., binary
	 * operator "-" also be unary operator if it is used as a sign.
	 *
	 * @return	True if the component may be unary sign, false otherwise.
	 */
	public boolean isUnarySign() {
		return false;
	}
	/**
	 * Check if the component can be at first position in an expression.
	 *
	 * @return	True if the component can be at position, false otherwise.
	 */
	public boolean canBeFirst() {
		return true;
	}
	/**
	 * Check if the component can be at last position in an expression.
	 *
	 * @return	True if the component can be at position, false otherwise.
	 */
	public boolean canBeLast() {
		return true;
	}
	/**
	 * Return numerical value of the component.
	 *
	 * @return	Numerical value of the component
	 */
	public double getValue() {
		return 0;
	}
	/**
	 * Set components numerical value to the value given as parameter.
	 *
	 * @param value	Numerical value for the component.
	 */
	public void setValue(double value) {
		return;
	}
	/**
	 * Return precedence value of the component.
	 *
	 * @return	Precedence value of the component.
	 */
	public int precedenceValue() {
		return -1;
	}
	/**
	 * If the component is unary operator, perform the operation with the
	 * operand given as parameter.
	 *
	 * @param operand	Operand for the operator.
	 * @return	Result of the operation.
	 */
	public double execute(double operand) {
		return Double.NaN;
	}
	/**
	 * If the component is a binary operator, perform the operation with the
	 * operands given in as parameters.
	 *
	 * @param second	Second operand for the operator.
	 * @param first		First operand for the operator.
	 * @return	Result of the operation.
	 */
	public double execute(double second, double first) {
		return Double.NaN;
	}
}

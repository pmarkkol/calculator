/* SubtractionOperator.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator.expression;

/**
 * This class represents a subtraction operator.
 *
 * @author Paavo Markkola
 */
public class SubtractionOperator extends Operator {
	/**
	 * String presentation of the subtraction operator.
	 */
	private static String operator = "-";
	/**
	 * Static method that returns string presentation of the subtraction
	 * operator.
	 *
	 * @return	String presentation of the subtraction operator.
	 */
	public static String getOperator() {
		return operator;
	}
	/**
	 * Return precedence value of the subtraction operator.
	 *
	 * @return	Precedence value of the subtraction operator.
	 */
	@Override
	public int precedenceValue() {
		return 1;
	}
	/**
	 * Execute the subtraction operator for two operand that are given as
	 * parameter, and return the results.
	 *
	 * @param second	Second operand for the operator.
	 * @param first		First operand for the operator.
	 * @return	Result of the operation.
	 */
	@Override
	public double execute(double second, double first) {
		return (first - second);
	}
}

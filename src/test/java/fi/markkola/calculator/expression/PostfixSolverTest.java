package fi.markkola.calculator.expression;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.LinkedList;

import org.junit.Test;

public class PostfixSolverTest {
	private static final double DELTA = 1e-15;
	PostfixSolver postfixCalculator = new PostfixSolver();

	private void checkErrorStatus(PostfixSolver calc, boolean status,
								  int statusPosition, String statusMessage) {
		assertEquals(status, calc.getStatus());
		assertEquals(statusPosition, calc.getPosition());
		assertEquals(statusMessage, calc.getMessage());
	}

	@Test
	public void testCreateEquatioParserEmptyEquation() throws Exception {
		assertNotNull(postfixCalculator);
		checkErrorStatus(postfixCalculator, false, 0, "Invalid expression string");
	}

	@Test
	public void testSetAndParseInvalidEquation() throws Exception {
		try {
			postfixCalculator.parseExpression("");
		} catch (InvalidExpressionException e) {}
		checkErrorStatus(postfixCalculator, false, 0, "Invalid expression string");
	}

	@Test
	public void testSetAndParseValidEquation() throws Exception {
		postfixCalculator.parseExpression("x+1");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
	}

	@Test
	public void testSingleValueEquation() throws Exception {
		postfixCalculator.parseExpression("1");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		LinkedList<String> infix = postfixCalculator.getInfixExpression();
		assertEquals("1.0", infix.get(0));
		assertEquals(1, infix.size());
	}

	@Test
	public void testNegativeValueInEquation() throws Exception {
		postfixCalculator.parseExpression("(-656)");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		LinkedList<String> infix = postfixCalculator.getInfixExpression();
		assertEquals("-656.0", infix.get(0));
		assertEquals(1, infix.size());
	}

	@Test
	public void testPositiveDoubleInEquation() throws Exception {
		postfixCalculator.parseExpression("123.456");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		LinkedList<String> infix = postfixCalculator.getInfixExpression();
		assertEquals("123.456", infix.get(0));
		assertEquals(1, infix.size());
	}

	@Test
	public void testNegativeDoubleInEquation() throws Exception {
		postfixCalculator.parseExpression("( - 123.456 )");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		LinkedList<String> infix = postfixCalculator.getInfixExpression();
		assertEquals("-123.456", infix.get(0));
		assertEquals(1, infix.size());
	}

	@Test
	public void testDoubleWithoutLeadingDigitsInEquation() throws Exception {
		postfixCalculator.parseExpression(".123");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		LinkedList<String> infix = postfixCalculator.getInfixExpression();
		assertEquals("0.123", infix.get(0));
		assertEquals(1, infix.size());
	}

	@Test
	public void testBinaryOperatorsInEquation() throws Exception {
		postfixCalculator.parseExpression("1+2-(3*4/5)^6");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		LinkedList<String> infix = postfixCalculator.getInfixExpression();
		assertEquals("1.0", infix.get(0));
		assertEquals("+", infix.get(1));
		assertEquals("2.0", infix.get(2));
		assertEquals("-", infix.get(3));
		assertEquals("(", infix.get(4));
		assertEquals("3.0", infix.get(5));
		assertEquals("*", infix.get(6));
		assertEquals("4.0", infix.get(7));
		assertEquals("/", infix.get(8));
		assertEquals("5.0", infix.get(9));
		assertEquals(")", infix.get(10));
		assertEquals("^", infix.get(11));
		assertEquals("6.0", infix.get(12));
		assertEquals(13, infix.size());
	}

	@Test
	public void testSpacesInEquation() throws Exception {
		postfixCalculator.parseExpression(" 1   +  2 ^ 3 * 4     /5  ");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		LinkedList<String> infix = postfixCalculator.getInfixExpression();
		assertEquals("1.0", infix.get(0));
		assertEquals("+", infix.get(1));
		assertEquals("2.0", infix.get(2));
		assertEquals("^", infix.get(3));
		assertEquals("3.0", infix.get(4));
		assertEquals("*", infix.get(5));
		assertEquals("4.0", infix.get(6));
		assertEquals("/", infix.get(7));
		assertEquals("5.0", infix.get(8));
		assertEquals(9, infix.size());
	}

	@Test
	public void testVariableXInEquation() throws Exception {
		postfixCalculator.parseExpression("x^5+588");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		LinkedList<String> infix = postfixCalculator.getInfixExpression();
		assertEquals("x", infix.get(0));
		assertEquals("^", infix.get(1));
		assertEquals("5.0", infix.get(2));
		assertEquals("+", infix.get(3));
		assertEquals("588.0", infix.get(4));
		assertEquals(5, infix.size());
	}

	@Test
	public void testNegativeVariableXInEquation() throws Exception {
		postfixCalculator.parseExpression("5*(-x)-10");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		LinkedList<String> infix = postfixCalculator.getInfixExpression();
		assertEquals("5.0", infix.get(0));
		assertEquals("*", infix.get(1));
		assertEquals("(", infix.get(2));
		assertEquals("-'", infix.get(3));
		assertEquals("x", infix.get(4));
		assertEquals(")", infix.get(5));
		assertEquals("-", infix.get(6));
		assertEquals("10.0", infix.get(7));
		assertEquals(8, infix.size());
	}

	@Test
	public void testUnarySignInEquation() throws Exception {
		postfixCalculator.parseExpression("-x");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		LinkedList<String> infix = postfixCalculator.getInfixExpression();
		assertEquals("-'", infix.get(0));
		assertEquals("x", infix.get(1));
		assertEquals(2, infix.size());
	}

	@Test
	public void testUnarySignInEquationWithOperators() throws Exception {
		postfixCalculator.parseExpression("x^-2-+5");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		LinkedList<String> infix = postfixCalculator.getInfixExpression();
		assertEquals("x", infix.get(0));
		assertEquals("^", infix.get(1));
		assertEquals("-'", infix.get(2));
		assertEquals("2.0", infix.get(3));
		assertEquals("-", infix.get(4));
		assertEquals("+'", infix.get(5));
		assertEquals("5.0", infix.get(6));
		assertEquals(7, infix.size());
	}

	@Test
	public void testUnarySquareRootInEquation() throws Exception {
		postfixCalculator.parseExpression("sqr x");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		LinkedList<String> infix = postfixCalculator.getInfixExpression();
		assertEquals("sqr", infix.get(0));
		assertEquals("x", infix.get(1));
		assertEquals(2, infix.size());
	}

	@Test
	public void testUnaryAbsolutValueInEquation() throws Exception {
		postfixCalculator.parseExpression("abs x");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		LinkedList<String> infix = postfixCalculator.getInfixExpression();
		assertEquals("abs", infix.get(0));
		assertEquals("x", infix.get(1));
		assertEquals(2, infix.size());
	}

	@Test
	public void testUnaryOperatorsInEquation() throws Exception {
		postfixCalculator.parseExpression("sqr abs x");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		LinkedList<String> infix = postfixCalculator.getInfixExpression();
		assertEquals("sqr", infix.get(0));
		assertEquals("abs", infix.get(1));
		assertEquals("x", infix.get(2));
		assertEquals(3, infix.size());
	}

	@Test
	public void testConstantsInEquation() throws Exception {
		postfixCalculator.parseExpression("e*pi");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		LinkedList<String> infix = postfixCalculator.getInfixExpression();
		assertEquals("e", infix.get(0));
		assertEquals("*", infix.get(1));
		assertEquals("pi", infix.get(2));
		assertEquals(3, infix.size());
	}

	@Test
	public void testEqualitySignInEquation() throws Exception {
		postfixCalculator.parseExpression("y=x");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		LinkedList<String> infix = postfixCalculator.getInfixExpression();
		assertEquals("y=", infix.get(0));
		assertEquals("x", infix.get(1));
		assertEquals(2, infix.size());
	}

	@Test
	public void testEqualitySignInEquationWithSpaces() throws Exception {
		postfixCalculator.parseExpression("  y =    x");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		LinkedList<String> infix = postfixCalculator.getInfixExpression();
		assertEquals("y=", infix.get(0));
		assertEquals("x", infix.get(1));
		assertEquals(2, infix.size());
	}

	@Test
	public void testValidEquation() throws Exception {
		postfixCalculator.parseExpression("14*x+3");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		assertFalse(postfixCalculator.getInfixExpression().isEmpty());
	}

	@Test
	public void testInvalidEquation() throws Exception {
		try {
			postfixCalculator.parseExpression("14*x+3a");
		} catch (InvalidExpressionException e) {}
		checkErrorStatus(postfixCalculator, false, 6, "Unknown character in equation string");
		assertTrue(postfixCalculator.getInfixExpression().isEmpty());
	}

	@Test
	public void testInvalidEquationWithSpaces() throws Exception {
		try {
			postfixCalculator.parseExpression("14   * x+ 3   a");
		} catch (InvalidExpressionException e) {}
		checkErrorStatus(postfixCalculator, false, 14, "Unknown character in equation string");
		assertTrue(postfixCalculator.getInfixExpression().isEmpty());
	}

	@Test
	public void testRulesBinaryOperatorCannotBeFirst() throws Exception {
		try {
			postfixCalculator.parseExpression("*x+3");
		} catch (InvalidExpressionException e) {}
		checkErrorStatus(postfixCalculator, false, 0, "Invalid component in first position");
		assertTrue(postfixCalculator.getInfixExpression().isEmpty());
	}

	@Test
	public void testRulesRightParenthesisCannotBeFirst() throws Exception {
		try {
			postfixCalculator.parseExpression(")*x+3");
		} catch (InvalidExpressionException e) {}
		checkErrorStatus(postfixCalculator, false, 0, "Invalid component in first position");
		assertTrue(postfixCalculator.getInfixExpression().isEmpty());
	}

	@Test
	public void testRulesBinaryOperatorCannotBeLast() throws Exception {
		try {
			postfixCalculator.parseExpression("3*x+");
		} catch (InvalidExpressionException e) {}
		checkErrorStatus(postfixCalculator, false, 3, "Invalid component in last position");
		assertTrue(postfixCalculator.getInfixExpression().isEmpty());
	}

	@Test
	public void testRulesLeftParenthesisCannotBeLast() throws Exception {
		try {
			postfixCalculator.parseExpression("3*x+(");
		} catch (InvalidExpressionException e) {}
		checkErrorStatus(postfixCalculator, false, 4, "Invalid component in last position");
		assertTrue(postfixCalculator.getInfixExpression().isEmpty());
	}

	@Test
	public void testRulesEmptyParenthesesNotAllowed() throws Exception {
		try {
			postfixCalculator.parseExpression("3*x+()");
		} catch (InvalidExpressionException e) {}
		checkErrorStatus(postfixCalculator, false, 4, "Empty parentheses not allowed");
		assertTrue(postfixCalculator.getInfixExpression().isEmpty());
	}

	@Test
	public void testRulesRightParenthesesMustMatch() throws Exception {
		try {
			postfixCalculator.parseExpression("(((x))");
		} catch (InvalidExpressionException e) {}
		checkErrorStatus(postfixCalculator, false, 2, "Parentheses do not match");
		assertTrue(postfixCalculator.getInfixExpression().isEmpty());
	}

	@Test
	public void testRulesLeftParenthesesMustMatch() throws Exception {
		try {
			postfixCalculator.parseExpression("((x)))");
		} catch (InvalidExpressionException e) {}
		checkErrorStatus(postfixCalculator, false, 5, "Parentheses do not match");
		assertTrue(postfixCalculator.getInfixExpression().isEmpty());
	}

	@Test
	public void testRulesLeftParenthesesMustBeFirstMatch() throws Exception {
		try {
			postfixCalculator.parseExpression("3*)(4+1");
		} catch (InvalidExpressionException e) {}
		checkErrorStatus(postfixCalculator, false, 2, "Parentheses do not match");
		assertTrue(postfixCalculator.getInfixExpression().isEmpty());
	}

	@Test
	public void testRulesBinaryOperatorCannotBeNextToAnother() throws Exception {
		try {
			postfixCalculator.parseExpression("3+*5");
		} catch (InvalidExpressionException e) {}
		checkErrorStatus(postfixCalculator, false, 2, "Invalid operator position");
		assertTrue(postfixCalculator.getInfixExpression().isEmpty());
	}

	@Test
	public void testRulesOperatorCannotBeEnclosedInParentheses() throws Exception {
		try {
			postfixCalculator.parseExpression("3+(-)*5");
		} catch (InvalidExpressionException e) {}
		checkErrorStatus(postfixCalculator, false, 3, "Unary operator in wrong position");
		assertTrue(postfixCalculator.getInfixExpression().isEmpty());
	}

	@Test
	public void testRulesOperatorInsideLeftParenthesis() throws Exception {
		try {
			postfixCalculator.parseExpression("3*(/5*4)");
		} catch (InvalidExpressionException e) {}
		checkErrorStatus(postfixCalculator, false, 3, "Binary operator error with parenthesis");
		assertTrue(postfixCalculator.getInfixExpression().isEmpty());
	}

	@Test
	public void testRulesOperatorInsideRightParenthesis() throws Exception {
		try {
			postfixCalculator.parseExpression("(3+2*)5");
		} catch (InvalidExpressionException e) {}
		checkErrorStatus(postfixCalculator, false, 4, "Binary operator error with parenthesis");
		assertTrue(postfixCalculator.getInfixExpression().isEmpty());
	}

	@Test
	public void testRulesEqualityMustBeFirst() throws Exception {
		try {
			postfixCalculator.parseExpression("x+4*y=7");
		} catch (InvalidExpressionException e) {}
		checkErrorStatus(postfixCalculator, false, 4, "Equality sign must be at first position");
		assertTrue(postfixCalculator.getInfixExpression().isEmpty());
	}

	@Test
	public void testRulesOnlyOneEqualitySign() throws Exception {
		try {
			postfixCalculator.parseExpression("y=x+4*y=7");
		} catch (InvalidExpressionException e) {}
		checkErrorStatus(postfixCalculator, false, 6, "Equality sign must be at first position");
		assertTrue(postfixCalculator.getInfixExpression().isEmpty());
	}

	@Test
	public void testInfixtToPostfixSimple() throws Exception {
		postfixCalculator.parseExpression("1+2");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		LinkedList<String> postfix = postfixCalculator.getPostfixExpression();
		assertEquals("1.0", postfix.get(0));
		assertEquals("2.0", postfix.get(1));
		assertEquals("+", postfix.get(2));
	}

	@Test
	public void testInfixtToPostfixPrecedence() throws Exception {
		postfixCalculator.parseExpression("1+2/3-4*5");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		LinkedList<String> postfix = postfixCalculator.getPostfixExpression();
		assertEquals("1.0", postfix.get(0));
		assertEquals("2.0", postfix.get(1));
		assertEquals("3.0", postfix.get(2));
		assertEquals("/", postfix.get(3).toString());
		assertEquals("+", postfix.get(4));
		assertEquals("4.0", postfix.get(5));
		assertEquals("5.0", postfix.get(6));
		assertEquals("*", postfix.get(7));
		assertEquals("-", postfix.get(8));
	}

	@Test
	public void testInfixtToPostfixParentheses() throws Exception {
		postfixCalculator.parseExpression("((1+2)*(3-4))");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		LinkedList<String> postifx = postfixCalculator.getPostfixExpression();
		assertEquals("1.0", postifx.get(0));
		assertEquals("2.0", postifx.get(1));
		assertEquals("+", postifx.get(2));
		assertEquals("3.0", postifx.get(3));
		assertEquals("4.0", postifx.get(4));
		assertEquals("-", postifx.get(5));
		assertEquals("*", postifx.get(6));
	}

	@Test
	public void testInfixtToPostfixVariable() throws Exception {
		postfixCalculator.parseExpression("1+x");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		LinkedList<String> postifx = postfixCalculator.getPostfixExpression();
		assertEquals("1.0", postifx.get(0));
		assertEquals("x", postifx.get(1));
		assertEquals("+", postifx.get(2));
	}

	@Test
	public void testInfixtToPostfixUnaryOperators() throws Exception {
		postfixCalculator.parseExpression("sqr (abs x + 1)");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		LinkedList<String> postifx = postfixCalculator.getPostfixExpression();
		assertEquals("x", postifx.get(0));
		assertEquals("abs", postifx.get(1));
		assertEquals("1.0", postifx.get(2));
		assertEquals("+", postifx.get(3));
		assertEquals("sqr", postifx.get(4));
	}

	@Test
	public void testInfixtToPostfixUnaryOperatorsV2() throws Exception {
		postfixCalculator.parseExpression("sqr abs x");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		LinkedList<String> postfix = postfixCalculator.getPostfixExpression();
		assertEquals("x", postfix.get(0));
		assertEquals("abs", postfix.get(1));
		assertEquals("sqr", postfix.get(2));
	}

	@Test
	public void testInfixtToPostfixConstantE() throws Exception {
		postfixCalculator.parseExpression("e^x");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		LinkedList<String> postfix = postfixCalculator.getPostfixExpression();
		assertEquals("e", postfix.get(0));
		assertEquals("x", postfix.get(1));
		assertEquals("^", postfix.get(2));
	}

	@Test
	public void testInfixtToPostfixConstantPi() throws Exception {
		postfixCalculator.parseExpression("pi*x^2");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		LinkedList<String> postfix = postfixCalculator.getPostfixExpression();
		assertEquals("pi", postfix.get(0));
		assertEquals("x", postfix.get(1));
		assertEquals("2.0", postfix.get(2));
		assertEquals("^", postfix.get(3));
		assertEquals("*", postfix.get(4));
	}

	@Test
	public void testInfixtToPostfixEqualitySign() throws Exception {
		postfixCalculator.parseExpression("y=-x^2");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		LinkedList<String> postfix = postfixCalculator.getPostfixExpression();
		assertEquals("x", postfix.get(0));
		assertEquals("2.0", postfix.get(1));
		assertEquals("^", postfix.get(2));
		assertEquals("-'", postfix.get(3));
	}

	@Test
	public void testSolvePostfixInvalidEquation() throws Exception {
		PostfixSolver calc = new PostfixSolver();
		try {
			calc.evaluateExpression(0);
			fail("Exception expected");
		} catch (EvaluationErrorException e) {
			checkErrorStatus(calc, false, 0, "Could not evaluate expression");
		}
	}

	@Test
	public void testEvaluationErrorException() throws Exception {
		postfixCalculator.parseExpression("2x");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		try {
			postfixCalculator.evaluateExpression(0);
			fail("Exception expected");
		} catch (EvaluationErrorException e) {
			checkErrorStatus(postfixCalculator, false, 0, "Could not evaluate expression");
		}
	}

	@Test
	public void testSolvePostfixSingleNumber() throws Exception {
		postfixCalculator.parseExpression("1");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		assertEquals(1, postfixCalculator.evaluateExpression(0), DELTA);
		assertEquals(1, postfixCalculator.evaluateExpression(1), DELTA);
		assertEquals(1, postfixCalculator.evaluateExpression(10), DELTA);
	}

	@Test
	public void testSolvePostfixVariable() throws Exception {
		postfixCalculator.parseExpression("x");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		assertEquals(0, postfixCalculator.evaluateExpression(0), DELTA);
		assertEquals(1, postfixCalculator.evaluateExpression(1), DELTA);
		assertEquals(10, postfixCalculator.evaluateExpression(10), DELTA);
	}

	@Test
	public void testSolvePostfixVariableWithUnarySign() throws Exception {
		postfixCalculator.parseExpression("-x");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		assertEquals(0, postfixCalculator.evaluateExpression(0), DELTA);
		assertEquals(-1, postfixCalculator.evaluateExpression(1), DELTA);
		assertEquals(-10, postfixCalculator.evaluateExpression(10), DELTA);
	}

	@Test
	public void testSolvePostfixVariableWithUnarySignAndOperator() throws Exception {
		postfixCalculator.parseExpression("-x^-2-+5");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		assertEquals(Double.NEGATIVE_INFINITY, postfixCalculator.evaluateExpression(0), DELTA);
		assertEquals(-5.25, postfixCalculator.evaluateExpression(2), DELTA);
		assertEquals(-5.01, postfixCalculator.evaluateExpression(10), DELTA);
	}

	@Test
	public void testSolvePostfixVariableInEquation() throws Exception {
		postfixCalculator.parseExpression("x+1");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		assertEquals(1, postfixCalculator.evaluateExpression(0), DELTA);
		assertEquals(2, postfixCalculator.evaluateExpression(1), DELTA);
		assertEquals(11, postfixCalculator.evaluateExpression(10), DELTA);
	}

	@Test
	public void testSolvePostfixComplexEquation() throws Exception {
		postfixCalculator.parseExpression("(x+4)*(x-10)/5");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		assertEquals(-8, postfixCalculator.evaluateExpression(0), DELTA);
		assertEquals(-9, postfixCalculator.evaluateExpression(1), DELTA);
		assertEquals(0, postfixCalculator.evaluateExpression(10), DELTA);
	}

	@Test
	public void testSolvePostfixPower() throws Exception {
		postfixCalculator.parseExpression("x^2");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		assertEquals(0, postfixCalculator.evaluateExpression(0), DELTA);
		assertEquals(1, postfixCalculator.evaluateExpression(1), DELTA);
		assertEquals(100, postfixCalculator.evaluateExpression(10), DELTA);
	}

	@Test
	public void testSolveUnaryOperators() throws Exception {
		postfixCalculator.parseExpression("sqr (abs x + 1)");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		assertEquals(2.46981780704569380, postfixCalculator.evaluateExpression(-5.1), DELTA);
		assertEquals(1.41421356237309504, postfixCalculator.evaluateExpression(-1), DELTA);
		assertEquals(1.0, postfixCalculator.evaluateExpression(0), DELTA);
		assertEquals(1.73205080756887729, postfixCalculator.evaluateExpression(2), DELTA);
		assertEquals(2.34520787991171477, postfixCalculator.evaluateExpression(4.5), DELTA);
	}

	@Test
	public void testSolvePostfixRationalNumbers() throws Exception {
		postfixCalculator.parseExpression("x*1/3");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		assertEquals(0, postfixCalculator.evaluateExpression(0), DELTA);
		assertEquals(0.3333333333333333, postfixCalculator.evaluateExpression(1), DELTA);
		assertEquals(1.3333333333333333, postfixCalculator.evaluateExpression(4), DELTA);
	}

	@Test
	public void testSolvePostfixDivideByZero() throws Exception {
		postfixCalculator.parseExpression("1/(x-1)");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		assertEquals(-1, postfixCalculator.evaluateExpression(0), DELTA);
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		assertEquals(Double.POSITIVE_INFINITY, postfixCalculator.evaluateExpression(1), DELTA);
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		assertEquals(0.3333333333333333, postfixCalculator.evaluateExpression(4), DELTA);
		checkErrorStatus(postfixCalculator, true, 0, "Success");
	}

	@Test
	public void testSolvePostfixWithConstantE() throws Exception {
		postfixCalculator.parseExpression("e^x");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		assertEquals(0.04978706836786395, postfixCalculator.evaluateExpression(-3), DELTA);
		assertEquals(0.36787944117144233, postfixCalculator.evaluateExpression(-1), DELTA);
		assertEquals(1, postfixCalculator.evaluateExpression(0), DELTA);
		assertEquals(7.3890560989306495, postfixCalculator.evaluateExpression(2), DELTA);
		assertEquals(22026.465794806703, postfixCalculator.evaluateExpression(10), DELTA);
	}

	@Test
	public void testSolvePostfixWithConstantPi() throws Exception {
		postfixCalculator.parseExpression("pi*x^2");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		assertEquals(12.5663706143591729538, postfixCalculator.evaluateExpression(-2), DELTA);
		assertEquals(0, postfixCalculator.evaluateExpression(0), DELTA);
		assertEquals(3.14159265358979323, postfixCalculator.evaluateExpression(1), DELTA);
		assertEquals(78.5398163397448309615, postfixCalculator.evaluateExpression(5), DELTA);
	}

	@Test
	public void testSolvePostfixWithEqualitySign() throws Exception {
		postfixCalculator.parseExpression("y=-x^2");
		checkErrorStatus(postfixCalculator, true, 0, "Success");
		assertEquals(-4, postfixCalculator.evaluateExpression(-2), DELTA);
		assertEquals(0, postfixCalculator.evaluateExpression(0), DELTA);
		assertEquals(-1, postfixCalculator.evaluateExpression(1), DELTA);
		assertEquals(-25, postfixCalculator.evaluateExpression(5), DELTA);
	}

	@Test
	public void testGetUserInstructions() throws Exception {
		assertNotNull(postfixCalculator.getUserInstructions());
	}
}

/* GraphLine.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator.graph;

/**
 * This class represents a line on a Cartesian coordinate system, with x and y
 * axes.
 *
 * @author Paavo Markkola
 */
public class GraphLine {
	/**
	 * X coordinate of the start point of the line.
	 */
	public int x1;
	/**
	 * Y coordinate of the start point of the line.
	 */
	public int y1;
	/**
	 * X coordinate of the end point of the line.
	 */
	public int x2;
	/**
	 * Y coordinate of the end point of the line.
	 */
	public int y2;
	/**
	 * Create a new GraphLine that starts at point at x1,y1 and ends at
	 * point x2,y2.
	 *
	 * @param x1	X coordinate of the start point of the line.
	 * @param y1	Y coordinate of the start point of the line.
	 * @param x2	X coordinate of the end point of the line.
	 * @param y2	Y coordinate of the end point of the line.
	 */
	public GraphLine(int x1, int y1, int x2, int y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}
}

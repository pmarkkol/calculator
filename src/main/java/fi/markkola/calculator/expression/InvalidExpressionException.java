/* InvalidExpressionException.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator.expression;

@SuppressWarnings("serial")
/**
 * This class is an exception that is thrown when mathematical expression being
 * parsed is invalid and cannot be fully parsed as such.
 *
 * @author Paavo Markkola
 */
public class InvalidExpressionException extends RuntimeException {

	private String errorMessage;
	private String errorExpression;
	private int errorPosition;
	/**
	 * Construct a new InvalidExpressionException with specific message.
	 *
	 * @param message	Message describing the cause for the exception.
	 */
	public InvalidExpressionException(String message) {
		super(message);
		errorMessage = message;
		errorExpression = "";
		errorPosition = 0;
	}
	/**
	 * Construct a new InvalidExpressionException with parameters from which
	 * the message for the exception is formed.
	 *
	 * @param message		Base message describing the cause for the exception.
	 * @param expression	Expression which caused the exceptions.
	 * @param position		Position within the expression that caused the
	 * 						exception.
	 */
	public InvalidExpressionException(String message, String expression, int position) {
		super(message);
		errorMessage = message;
		errorExpression = expression;
		errorPosition = position;
	}
	/**
	 * Override the default getMessage method and return message specific for
	 * InvalidExpressionException class.
	 */
	@Override
	public String getMessage() {
		return errorMessage;
	}
	/**
	 * Returns the expression string that could not parsed and caused this
	 * exception.
	 *
	 * @return	The problematic expression string.
	 */
	public String getExpression() {
		return errorExpression;
	}
	/**
	 * Return exact position of the error within the expression string that
	 * caused this exception.
	 *
	 * @return	Position of the error within the expression string.
	 */
	public int getPosition() {
		return errorPosition;
	}
}

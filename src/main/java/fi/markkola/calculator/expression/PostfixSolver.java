/* PostfixSolver.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator.expression;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class to parse and evaluate mathematical expressions of format y = f(x).
 * <p>
 * Mathematical expressions must be given in infix notation. Converts these
 * expressions to reverse polish notation, i.e., postfix notation. Evaluations
 * are performed in postfix notation.
 *
 * @author Paavo Markkola
 */
public class PostfixSolver implements ExpressionSolver {
	/**
	 * Instruction string for the user describing how to use the feature.
	 */
	private String instructionsHeader =
		  "<div class=\"ins-header\">"
		+ "<p>"
		+ "Expressions must follow infix notation, e.g, "
		+ "<span class=\"ins-code\">(x+1)*2</span>. Expressions are converted "
		+ "to reverse polish notation, i.e., postfix notation. Evaluations "
		+ "of expressions are performed using the postfix notation."
		+ "<p>"
		+ "</div>";
	/**
	 * Instruction string to show examples on how to use the software.
	 */
	private String instructionsExamples =
		  "<div class=\"ins-examples\">"
		+ "<p>Examples:</p>"
		+ "<ul class=\"ins-list\">"
		+ "<li>To plot a vertical parabola that opens downwards enter the "
		+ "expression <span class=\"ins-code\">-x^2</span>.</li>"
		+ "<li>To plot exponential function enter expresion "
		+ "<span class=\"ins-code\">e^x</span>.</li>"
		+ "</ul>"
		+ "</div>";
	/**
	 * Infix notation of the expression divided to a list of components.
	 */
	private List<Component> infixExpression = new ArrayList<>();
	/**
	 * Infix notation of the expression in stack format. Stack is needed for
	 * converting infix notation to postfix notation.
	 */
	private LinkedList<Component> infixStack = new LinkedList<>();
	/**
	 * Postfix notation of the expression in stack format. This stack is used
	 * to evaluate the expression.
	 */
	private LinkedList<Component> postfixStack = new LinkedList<>();
	/**
	 * Stacks needed for transforming infix stack to postfix stack.
	 */
	private LinkedList<Component> operatorStack;
	private LinkedList<Component> workStack;
	private LinkedList<Component> sourceStack;
	/**
	 * Stack needed to evaluate postfix stack.
	 */
	private LinkedList<Double> valueStack;
	/**
	 * Data structure to hold status of parse and evaluate operations.
	 */
	private SolverStatus status = new SolverStatus();
	/**
	 * The original expression in string presentation.
	 */
	private String originalExpression;
	/**
	 * Remaining expression that has not yet been parsed or cannot be parsed in
	 * case of error.
	 */
	private String unparsedExpression;
	/**
	 * Position of the remaining unparsed expression in relation to the
	 * original expression.
	 */
	private int unparsedPosition = 0;
	/**
	 * Construct new PostfixSolver with empty string for expression.
	 */
	public PostfixSolver() {
		initExpressionParser("");
		status.set(false, 0, "Invalid expression string");
	}
	/**
	 * Build instructions string for components.
	 *
	 * @return	User instructions for components.
	 */
	private String getComponentInstructions() {
		String str =
			  "<div class=\"ins-components\">"
			+ "<p>Valid expresions may consist the following components:</p>"
			+ "<ul class=\"ins-list\">";
		for (ComponentType type : ComponentType.values())
			str += "<li>" + type.getUserInstructions() + "</li>";
		return str + "</ul></div>";
	}
	/**
	 * Format and return user instructions string.
	 *
	 * @return Instructions for the user in String presentation.
	 */
	@Override
	public String getUserInstructions() {
		String retVal = "<div class=\"ins-main\">";
		retVal += instructionsHeader;
		retVal += getComponentInstructions();
		retVal += instructionsExamples;
		retVal += "</div>";
		return retVal;
	}
	/**
	 * Parse a given expression string in infix annotation and transform it to
	 * postfix annotation.
	 *
	 * @param expression	String presentation of the expression in infix
	 * 						annotation.
	 * @exception InvalidExpressionException If error was encountered during
	 * 										parsing.
	 */
	@Override
	public void parseExpression(String expression)
											throws InvalidExpressionException {
		initExpressionParser(expression);
		parseInfixExpression();
		checkUnarySigns();
		validateInfixExpression();
		infixToPostfix();
		if (!status.getStatus())
			throw new InvalidExpressionException(status.getMessage(),
											   originalExpression,
											   status.getPosition());
	}
	/**
	 * Initialize the parser.
	 *
	 * @param equation	Expression to be parsed.
	 */
	private void initExpressionParser(String expression) {
		setInfixExpression(expression);
		infixExpression.clear();
		infixStack.clear();
		postfixStack.clear();
		status.set(true, 0, "Success");
	}
	/**
	 * Store given equation string and reset related parsing data.
	 *
	 * @param expression
	 */
	private void setInfixExpression(String expression) {
		originalExpression = expression;
		unparsedExpression = expression;
		unparsedPosition = 0;
	}
	/**
	 * Parse expression in infix notation. Search for components, e.g., numbers
	 * and operators in the expression. Store each found component to a list
	 * for easy access.
	 */
	private void parseInfixExpression() {
		Component component;
		while (true) {
			if ((component = findEquationComponent()) != null) {
				infixExpression.add(component);
				unparsedPosition += component.getLength();
			} else
				return;
		}
	}
	/**
	 * Go through the list of different expression components and see if any of
	 * the regular expressions match to the start of the remaining expression.
	 * If match is found then create a new component of the matching type and
	 * return it.
	 *
	 * @return	Component found from the remaining unparsed expression.
	 */
	private Component findEquationComponent() {
		for (ComponentType type : ComponentType.values()) {
			String component = parseExpressionComponent(type.getRegEx());
			if (component != null)
				return type.create(component, unparsedPosition);
		}
		return null;
	}
	/**
	 * Try to match the regular expression given as parameter to the remaining
	 * unparsed expression. If there is a substring that matches then remove
	 * this substring from the expression and return it.
	 *
	 * @param regEx	Regular expression of a component to be searched.
	 * @return	Found substring if the was a match, null otherwise.
	 */
	private String parseExpressionComponent(String regEx) {
		Matcher matcher = Pattern.compile(regEx).matcher(unparsedExpression);
		if (matcher.find()) {
			String component = unparsedExpression.substring(matcher.start(),
														  matcher.end());
			unparsedExpression = matcher.replaceFirst("");
			return component;
		}
		return null;
	}
	/**
	 * Change binary operators - and + to unary operators if needed.
	 * <p>
	 * Go through the parsed list of expression components and locate binary
	 * operators. If the binary operator is either - or +, then change the
	 * operator to unary operator if it matches criteria for the change.
	 */
	private void checkUnarySigns() {
		for (int index = 0; index < infixExpression.size(); index++)
			if (needToChangeToUnarySign(index))
				infixExpression.set(index,
					new UnaryComponent(
									infixExpression.get(index).toString(),
									infixExpression.get(index).getPosition()));
	}
	/**
	 * Check if binary operators - and + should be unary signs instead.
	 * <p>
	 * Use unary operator instead of binary operator is at position where it
	 * cannot be used.
	 *
	 * @param index		Index of the binary operator in infixEquation list.
	 * @return			True if it change is needed and false if not.
	 */
	private boolean needToChangeToUnarySign(int index) {
		if (!infixExpression.get(index).isBinaryOperator() ||
			!infixExpression.get(index).isUnarySign())
			return false;
		if (index == 0)
			return true;
		if (infixExpression.get(index - 1).isEquality() ||
			infixExpression.get(index - 1).isLeftParenthesis() ||
			infixExpression.get(index - 1).isUnaryOperator() ||
			infixExpression.get(index - 1).isBinaryOperator())
			return true;
		return false;
	}
	/**
	 * Check that the parsed infix expression is valid. If not remove the
	 * content of the infix list.
	 */
	private void validateInfixExpression() {
		if ((!isValidExpression()) ||
			(!isFullyParsed()) ||
			(!isValidInfixExpression()))
			infixExpression.clear();
	}
	/**
	 * Verify that the parsed infix is not an empty list. If it is then set an
	 * error about it.
	 *
	 * @return	True is infix list is not empty, false otherwise.
	 */
	private boolean isValidExpression() {
		if (!infixExpression.isEmpty())
			return true;
		status.set(false, 0, "Invalid expression string");
		return false;
	}
	/**
	 * Verify that all of the expression string was parsed. If parts of the
	 * expression string remain then something went wrong in parsing. If that
	 * is the case then set an error pointing to the remainder.
	 *
	 * @return	True if expression string was fully parsed, false otherwise.
	 */
	private boolean isFullyParsed() {
		if (unparsedExpression.isEmpty())
			return true;
		status.set(false, originalExpression.indexOf(unparsedExpression),
			 	 		  "Unknown character in equation string");
		return false;
	}
	/**
	 * Verify that the parsed infix expression follows certain rules so that
	 * the postfix notation can be created.
	 *
	 * @return True if all rules are followed, false otherwise.
	 */
	private boolean isValidInfixExpression() {
		if (!checkFirstComponent()) { return false; }
		if (!checkLastComponent()) { return false; }
		if (!checkEqualitySign()) { return false; }
		if (!checkEmptyParentheses()) { return false; }
		if (!checkParenthesesMatch()) { return false; }
		if (!checkPairedOperators()) { return false; }
		if (!checkEnclosedOperator()) { return false; }
		if (!checkUnaryOperator()) { return false; }
		return true;
	}
	/**
	 * Check if the component at first position can be at first position.
	 *
	 * @return	True if component can be at first position, false otherwise.
	 */
	private boolean checkFirstComponent() {
		if (infixExpression.get(0).canBeFirst())
			return true;
		status.set(false, infixExpression.get(0).getPosition(),
						  "Invalid component in first position");
		return false;
	}
	/**
	 * Check if the component at last position can be at last position.
	 *
	 * @return	True if component can be at first position, false otherwise.
	 */
	private boolean checkLastComponent() {
		if (infixExpression.get(infixExpression.size() - 1).canBeLast())
			return true;
		status.set(
				false,
				infixExpression.get(infixExpression.size() - 1).getPosition(),
				"Invalid component in last position");
		return false;
	}
	/**
	 * Check if equality sign is at first position if it is included at all.
	 *
	 * @return	True if equality sign is at correct position.
	 */
	private boolean checkEqualitySign() {
		for (int index = 0; index < infixExpression.size(); index++)
			if (infixExpression.get(index).isEquality() && index != 0) {
				status.set(false, infixExpression.get(index).getPosition(),
						   		  "Equality sign must be at first position");
				return false;
			}
		return true;
	}
	/**
	 * Check that the expression does not contain empty parentheses.
	 *
	 * @return	True if there are no empty parentheses, false otherwise.
	 */
	private boolean checkEmptyParentheses() {
		for (int index = 0; index < infixExpression.size(); index++)
			if (infixExpression.get(index).isLeftParenthesis())
				if ((index + 1 < infixExpression.size()) &&
					(infixExpression.get(index + 1).isRightParenthesis())) {
					status.set(false,
							   infixExpression.get(index).getPosition(),
							   "Empty parentheses not allowed");
					return false;
				}
		return true;
	}
	/**
	 * Check that the parentheses in expression match.
	 *
	 * @return	True parentheses match and false if not.
	 */
	private boolean checkParenthesesMatch() {
		int parenthesesBalance = 0, errorIndex = 0;
		for (int index = 0; index < infixExpression.size(); index++)
			if (infixExpression.get(index).isLeftParenthesis()) {
				parenthesesBalance++;
				errorIndex = index;
			} else if (infixExpression.get(index).isRightParenthesis()) {
				if (--parenthesesBalance < 0) {
					errorIndex = index;
					break;
				}
			}
		if (parenthesesBalance != 0) {
			status.set(false, errorIndex, "Parentheses do not match");
			return false;
		}
		return true;
	}
	/**
	 * Check that the expression does contain consecutive binary operators.
	 *
	 * @return	True if there are no consecutive binary operators,
	 * 			false otherwise.
	 */
	private boolean checkPairedOperators() {
		for (int index = 0; index < infixExpression.size(); index++)
			if (infixExpression.get(index).isBinaryOperator())
				if ((index + 1 < infixExpression.size()) &&
					(infixExpression.get(index + 1).isBinaryOperator())) {
					status.set(false,
							   infixExpression.get(index + 1).getPosition(),
							   "Invalid operator position");
					return false;
				}
		return true;
	}
	/**
	 * Check that an operator is not enclosed in parentheses alone.
	 *
	 * @return	True if operator is not enclosed and false otherwise.
	 */
	private boolean checkEnclosedOperator() {
		for (int index = 0; index < infixExpression.size(); index++)
			if (infixExpression.get(index).isBinaryOperator())
				if (((index - 1 >= 0) &&
					 (infixExpression.get(index - 1).isLeftParenthesis())) ||
					((index + 1 < infixExpression.size()) &&
					 (infixExpression.get(index + 1).isRightParenthesis()))) {
					status.set(false,
							   infixExpression.get(index).getPosition(),
							   "Binary operator error with parenthesis");
					return false;
				}
			else if (infixExpression.get(index).isUnaryOperator())
				if (((index - 1 >= 0) &&
					 (infixExpression.get(index - 1).isLeftParenthesis())) &&
					((index + 1 < infixExpression.size()) &&
					 (infixExpression.get(index + 1).isRightParenthesis()))) {
					status.set(false,
							   infixExpression.get(index).getPosition(),
							   "Unary operator error with parenthesis");
					return false;
				}
		return true;
	}
	/**
	 * Check that unary operators are not before binary operators or right
	 * parenthesis.
	 *
	 * @return	True if unary operators are OK and false if not.
	 */
	private boolean checkUnaryOperator() {
		for (int index = 0; index < infixExpression.size(); index++)
			if (infixExpression.get(index).isUnaryOperator())
				if ((index + 1 < infixExpression.size()) &&
					(infixExpression.get(index + 1).isBinaryOperator() ||
					 infixExpression.get(index + 1).isRightParenthesis())) {
					status.set(false,
							   infixExpression.get(index).getPosition(),
							   "Unary operator in wrong position");
					return false;
				}
		return true;
	}
	/**
	 * Perform the infix to postfix transformation.
	 * <p>
	 * Start by filling the infix stack from the parsed expression and
	 * initialize work stacks. Go through the infix stack handle each type of
	 * component separately. Finalize the result by filling the postfix stack
	 * from the work stacks.
	 */
	private void infixToPostfix() {
		fillInfixStack();
		infixToPostfixInitStacks();
		while (!sourceStack.isEmpty()) {
			Component component = sourceStack.pop();
			if (component.isEquality())
				infixToPostfixEquality(component);
			else if (component.isNumber() || component.isVariable() ||
					 component.isConstant())
				infixToPostfixNumeric(component);
			else if (component.isBinaryOperator())
				infixToPostfixBinaryOperator(component);
			else if (component.isUnaryOperator())
				infixToPostfixUnaryOperator(component);
			else if (component.isLeftParenthesis())
				infixToPostfixLeftParenthesis(component);
			else if (component.isRightParenthesis())
				infixToPostfixRightParenthesis();
			else
				break;
		}
		finalizePostfixStack();
	}
	/**
	 * Fill infix stack from the parsed infix expression.
	 */
	private void fillInfixStack() {
		for (int index = infixExpression.size(); index > 0; index--) {
			infixStack.push(infixExpression.get(index - 1));
		}
	}
	/**
	 * Initialize temporary work stacks used during the process. Copy infix
	 * stack to source stack so that the original infix stack will not be
	 * consumed during the process.
	 */
	private void infixToPostfixInitStacks() {
		operatorStack = new LinkedList<>();
		workStack = new LinkedList<>();
		sourceStack = new LinkedList<>();
		sourceStack.addAll(infixStack);
	}
	/**
	 * Handle equality sign component. These components are ignored.
	 *
	 * @param component	Equality sign component to be handled.
	 */
	private void infixToPostfixEquality(Component component) {

	}
	/**
	 * Handle number or variable component. These components go straight to the
	 * work stack as they appear.
	 *
	 * @param component	Number or variable component to be handled.
	 */
	private void infixToPostfixNumeric(Component component) {
		workStack.push(component);
	}
	/**
	 * Handle binary operator component. These components are pushed temporarily
	 * to the operator stack. If the operator has at the top components with
	 * higher precedence value then these components are all first moved to the
	 * work stack.
	 * <p>
	 * Special case is when unary sign, i.e., - or +, precedes ^ operator. In
	 * this case exponent takes precedence, e.g., -x^2 is same as -(x^2).
	 *
	 * @param component	Binary operator component to be handled.
	 */
	private void infixToPostfixBinaryOperator(Component component) {
		while (!operatorStack.isEmpty()) {
			if ((component.toString().equals("^")) &&
				((operatorStack.peek().isUnaryOperator()) &&
				 ((operatorStack.peek().toString().equals("-'")) ||
				  (operatorStack.peek().toString().equals("+'"))))) {
				operatorStack.push(component);
				return;
			} else
				if (operatorStack.peek().precedenceValue() >=
												component.precedenceValue())
					workStack.push(operatorStack.pop());
				else
					break;
		}
		operatorStack.push(component);
	}
	/**
	 * Handle unary operator component. These components are pushed temporarily
	 * to the operator stack. If the operator has at the top components with
	 * higher precedence value then these components are all first moved to the
	 * work stack.
	 *
	 * @param component	Unary operator component to be handled.
	 */
	private void infixToPostfixUnaryOperator(Component component) {
		while ((!operatorStack.isEmpty()) &&
			   (operatorStack.peek().precedenceValue() >
			   									component.precedenceValue()))
			workStack.push(operatorStack.pop());
		operatorStack.push(component);
	}
	/**
	 * Handle left parenthesis component. Left parenthesis components are
	 * pushed as such to the operator stack.
	 *
	 * @param component		Left parenthesis component to be handled.
	 */
	private void infixToPostfixLeftParenthesis(Component component) {
		operatorStack.push(component);
	}
	/**
	 * Handle right parenthesis component. Right parenthesis components cause
	 * the components on the operator stack to be moved to the work stack until
	 * a left parenthesis component is at the top. At this point both
	 * parentheses components are removed.
	 */
	private void infixToPostfixRightParenthesis() {
		while (!operatorStack.isEmpty())
			if (!operatorStack.peek().isLeftParenthesis())
				workStack.push(operatorStack.pop());
			else {
				operatorStack.pop();
				break;
			}
	}
	/**
	 * Finalize postfix transformation by copying work stack to the postfix
	 * stack. Before that empty remaining operators to the work stack.
	 */
	private void finalizePostfixStack() {
		while (!operatorStack.isEmpty())
			workStack.push(operatorStack.pop());
		while (!workStack.isEmpty())
			postfixStack.push(workStack.pop());
	}
	/**
	 * Evaluate expression using reverse polish notation with variable value
	 * given in parameter.
	 *
	 * @param variable	Value to be used for variables in expression.
	 * @throws EvaluationErrorException When expression cannot be evaluated.
	 */
	@Override
	public double evaluateExpression(double variable)
											throws EvaluationErrorException {
		evaluateEquationInitStacks();
		while (!sourceStack.isEmpty()) {
			Component component = sourceStack.pop();
			if (component.isNumber())
				evaluateNumberComponent(component);
			else if (component.isVariable())
				evaluateVariableComponent(variable, component);
			else if (component.isConstant())
				evaluateConstantComponent(component);
			else if (component.isBinaryOperator())
				evaluateBinaryOperatorComponent(component);
			else if (component.isUnaryOperator())
				evaluateUnaryOperatorComponent(component);
		}

		double result = evaluateEquationGetReturnValue();
		if (!status.getStatus())
			throw new EvaluationErrorException(status.getMessage());
		else
			return result;
	}
	/**
	 * Initialize temporary stacks used for evaluating postfix stack. Copy
	 * postfix stack to source stack so that the original postfix infix stack
	 * will not be consumed during the process.
	 */
	private void evaluateEquationInitStacks() {
		valueStack = new LinkedList<>();
		sourceStack = new LinkedList<>();
		sourceStack.addAll(postfixStack);
	}
	/**
	 * Handle number component when evaluating postfix stack. Numbers can be
	 * used as such so push the numerical value straight to the value stack.
	 *
	 * @param component	Number component to be handled.
	 */
	private void evaluateNumberComponent(Component component) {
		valueStack.push(component.getValue());
	}
	/**
	 * Handle variable component when evaluating postfix stack. Variables needs
	 * to be replaced with the value assigned for variables. After that the
	 * value can pushed to the value stack.
	 *
	 * @param value	Value to be used for variable.
	 * @param component	Variable component to be handled.
	 */
	private void evaluateVariableComponent(double value,
										   Component component) {
		component.setValue(value);
		valueStack.push(component.getValue());
	}
	/**
	 * Handle constant component when evaluating postfix stack. Constant can be
	 * used as such so push the numerical value straight to the value stack.
	 *
	 * @param component	Number component to be handled.
	 */
	private void evaluateConstantComponent(Component component) {
		valueStack.push(component.getValue());
	}
	/**
	 * Handle binary operator component when evaluating postfix stack. Binary
	 * operator needs two operands so pop two values from value stack. Perform
	 * the operation and store result to value stack.
	 *
	 *  @param component	Binary operator component to be handled.
	 */
	private void evaluateBinaryOperatorComponent(Component component) {
		valueStack.push(component.execute(valueStack.pop(), valueStack.pop()));
	}
	/**
	 * Handle unary operator component when evaluating postfix stack. Unary
	 * operator needs one operand so pop a value from value stack. Perform
	 * the operation and store result to value stack.
	 *
	 *  @param component	Unary operator component to be handled.
	 */
	private void evaluateUnaryOperatorComponent(Component component) {
		valueStack.push(component.execute(valueStack.pop()));
	}
	/**
	 * Get the top of value stack and use it as results for evaluation. If
	 * the stack is empty then set error.
	 *
	 * @return Top of the value stack. NaN if the stack is empty.
	 */
	private double evaluateEquationGetReturnValue() {
		if (valueStack.size() == 1) {
			status.set(true, 0, "Success");
			return valueStack.pop();
		} else {
			status.set(false, 0, "Could not evaluate expression");
			return Double.NaN;
		}
	}
	/**
	 * Get status of parse or evaluate operations.
	 *
	 * @return True for successful and false for failed operation.
	 */
	@Override
	public boolean getStatus() {
		return status.getStatus();
	}
	/**
	 * Get position in relation to the original equation as reference to the
	 * status of the parse or evaluate operations.
	 *
	 * @return Position related to the status of the operation.
	 */
	@Override
	public int getPosition() {
		return status.getPosition();
	}
	/**
	 * Get message describing the status of the parse or evaluate operations.
	 *
	 * @return Status message.
	 */
	@Override
	public String getMessage() {
		return status.getMessage();
	}
	/**
	 * Copy infix stack and return it.
	 *
	 * @return	Linked list containing infix stack.
	 */
	public LinkedList<String> getInfixExpression() {
		LinkedList<String> infix= new LinkedList<>();

		for (int index = 0; index < this.infixStack.size(); index++) {
			infix.add(this.infixStack.get(index).toString());
		}

		return infix;
	}
	/**
	 * Copy postfix stack and return it.
	 *
	 * @return	Linked list containing postfix stack.
	 */
	public LinkedList<String> getPostfixExpression() {
		LinkedList<String> postfix= new LinkedList<>();

		for (int index = 0; index < this.postfixStack.size(); index++) {
			postfix.add(this.postfixStack.get(index).toString());
		}

		return postfix;
	}
}

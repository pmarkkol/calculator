# Calculator #

Simple application in java to plot graphs of mathematical expressions of format y=f(x). For example, to plot a vertical parabola that opens downwards enter the expression '-x^2'.

### Requirements ###

 - Java Platform, Standard Edition 8. See http://www.oracle.com/technetwork/java/javase/overview/index.html for install instructions.
 - Maven. See https://maven.apache.org/ for more info.
 - git. See https://git-scm.com/ for more info.

### Instructions ####

Make sure that Java, maven and git are properly installed. To verify run commands

	javac -version 
	mvn --version
	git --version

In case of error check the intall instructions for these software. 

Checkout Calculator source from repository with command:

	git clone https://pmarkkol@bitbucket.org/pmarkkol/calculator.git

Move to the directory containing the source with

	cd calculator
	
Compile source using maven 
	
	mvn compile

Execute Calculator program with 

	mvn exec:java
	
Alternatively you can generate executable jar package with command:

	mvn package
 
The resulting jar file can be found from target directory. 

### Contact ###

Paavo Markkola (paavo.markkola@iki.fi)
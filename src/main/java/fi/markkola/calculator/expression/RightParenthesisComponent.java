/* RightParenthesisComponent.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.calculator.expression;

/**
 * This class represents a right parenthesis, i.e., ')', of a mathematical
 * expression. Parentheses can be used to signify a different precedence of
 * operators.
 *
 * @author Paavo Markkola
 */
public class RightParenthesisComponent extends Component {
	/**
	 * Instruction string for the user describing how to use the feature.
	 */
	private static String instructions =
		  "<span class=\"ins-strong\">Right parenthesis</span>, i.e., "
		+ "<span class=\"ins-code\">)</span>. Closes the parenthesis sequence.";
	/**
	 * Regular expression for right parenthesis components.
	 */
	public static String rightParenthesisRegEx = "(^\\s*[)]\\s*)";
	/**
	 * Construct a new RightParenthesisComponent.
	 *
	 * @param component	String presentation of the component.
	 * @param position	Position of the component in a mathematical expression.
	 */
	public RightParenthesisComponent(String component, int position) {
		super(component, position);
	}
	/**
	 * Format and return user instructions string.
	 *
	 * @return Instructions for the user in String presentation.
	 */
	public static String getUserInstructions() {
		return instructions;
	}
	/**
	 * Return regular expression for right parenthesis components.
	 *
	 * @return	Regular expression.
	 */
	public static String getRegEx() {
		return rightParenthesisRegEx;
	}
	/**
	 * Returns string presentation of the right parenthesis.
	 *
	 * @return	String presentation of the right parenthesis.
	 */
	@Override
	public String toString() {
		return ")";
	}
	/**
	 * This component is a right parenthesis so return true.
	 *
	 * @return	True.
	 */
	@Override
	public boolean isRightParenthesis() {
		return true;
	}
	/**
	 * Right parenthesis component can be at first position in an expression.
	 *
	 * @return	False.
	 */
	@Override
	public boolean canBeFirst() {
		return false;
	}
}
